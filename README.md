# **P**rotocol for **R**eliable **O**ne-Way **T**ransfer of **O**bjects (PROTO)


## Compiling

**With CMake**

    mkdir build; cd build; cmake .. -DCMAKE_BUILD_TYPE=Release; make; cd ..
The send-test and recv-test executable will then be found in the build dir

**The Hard Way**

Complie the receiver test file with the command:

    g++ -std=c++11 -Wall -O2 -lrt -pthread  -DNDEBUG proto/*.cpp recv-test.cpp -o recv-test

Compile the sender test file with the command:

    g++ -std=c++11 -Wall -O2 -lrt -pthread  -DNDEBUG proto/*.cpp send-test.cpp -o send-test

## Running
1. Connect to the server provided in the grading script http://www.csee.usf.edu/~kchriste/class2/projectGradingScript.pdf
2. Use ./recv-test port [outfile=out.dat] [loss=0.00] to run the reciever
3. Use ./send-test  my_port peer_ip peer_port [infile=in.dat [loss=0.00]] to run the sender.