Protocol for Reliable One-way Transfer of Objects (PROTO)
===============================================================================
:Authors:
  Kenneth Sills, Kevaughn Kerr
:Professor:
  Doctor Christensen
:Course:
  CNT 4004 - Computer Networks

Abstract
--------------------------------------------------------------------------------

Design Goals:

* The transfer of entire files (of arbitrary size) over an unreliable network
  (UDP|IP in this case).
* Proper multiplexing of connections on the same host over the same port number.
* Convergence to optimal network utilization.
* Congestion avoidance, TCP fairness.

What PROTO is not designed for:

* It's important to understand that PROTO is not designed for communication
  over a DTN or some such extremely resource-limited network.
* Security should be handled by the application or the underlying protocol, not
  this one.

Assumptions:

* Transmission of a full, single file, rather than an unknown data stream.
* The primary cause of packet loss on modern networks is congestion - packet
  loss caused by physical interference is much more rare.


Plan of Attack
--------------------------------------------------------------------------------
File Chunking
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
We approach the task of sending a file by splitting it into a predefined number
of statically-sized blocks. By maintaining lists of blocks (not) received, the
sender and receiver can send the file in an unordered block-by-block means.

This allows us to forego sequential data transfer and perform transfer from
front -> back regardless of the losses along the way. If the transfer is not
complete upon reaching EOF, the sender simply wraps around to the front of the
file and attempts to satisfy those blocks not already satisfied in the previous
pass.


Multiplexing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PROTO does not require the sender or receiver to maintain a consistent IP or
port number, the targets are free to change so long as the connection-specific
UIDs are constant. Each side maintains this value and will accept a packet to
a connection corresponding to that UID from any source. This allows a single
port listener to maintain a large number of simultaneous connections without
having to open a separate socket and allows devices to change their address
during connection without interfering.

*Note*: Future versions of PROTO may want to provide some means for a device
who has changed IP or port to inform the peer.

Transmission Process
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
When it comes to the transfer of data, PROTO is a three-phase system. It first
establishes a connection via two-way handshake, performs the data transfer, and
then agrees on termination with another two-way handshake (though one way is
optional).

Connection
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
The sender first sends a CREQ connection request to the receiver, containing a
unique ID for the sender-side multiplexing, the size of the file in bytes, and
the block size exponent modifier for the equation :math:`2^{7+b}`. Directly
proceeding this is an RAUG connection request augment packet, which includes a
randomly generated block ID that represents the block just before the first
block to be transmitted.

If in any of the above steps a packet is dropped or received in the wrong order,
the receiver will simply ignore the request and stay silent. This will prompt a
timeout at the sender host, which will reattempt up to it's user-set retry limit.
If all goes well but the filesize is too large for the receiver to accept, the
receiver sends back a connection response with the refuse bit set to 1.

On acceping the connection, the receiver generates it's own multiplexing UID.
A CREP connection packet is sent indicating success with the receiver's UID.
Directly after that, the receiver sends an ACK packet indicating the distance
between it's receiving of the CREQ and the RAUG packets.

Just like before, if one or both of these packets are lost the sender restarts
the process. The receiver will recognize this case when, after sending the
CREP and ACK packets it receives a connection request on the same UID. It can
simply retransmit these two packets, including the timing information.

Transfer
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Once the connection is fully established, the sender initializes a data structure
tracking the blocks that have yet to be acknowledged by the sender. It prepares
a burst of these DAT packets with the contents of these blocks of an appropriate
size for the current bandwidth estimate and user-defined burst duration. For
the number of blocks or the burst duration, whichever comes first, these packets
are sent out on the network.

The sender then stores information regarding the burst send time, the blocks
within the burst and the first and / or last block ID for use in analytics.

Each implementation is free to choose how the sender manages what blocks are
still required. In this reference implementation, we make use of an array
of (first_block, gap_length) nodes to show where gaps are in acknowledged data.
This allows us to severely reduce memory requirements and take advantage of the
"bursty" nature of networking noise / loss.

Upon arrival of a non-corrupted DAT, the receiver checks if this is a duplicate
or invalid block. If so, the block is ignored. Otherwise the block is accepted
and can be copied into the file (at the discretion of the implementation). A
map of timing offsets to bursts is kept to allow the receiver to provide a 0
offset on the first incoming block in a unique burst ID.  To inform the sender
of success, an acknowledge packet is produced with the sender's UID, the
block_id that was received, and a timeframe field of how many nanoseconds
separate this block from the first block received in it's burst ID.

If the receive is unable to produce a valid timing, it should opt instead to
transmit `std::numeric_limits<uint32_t>::max()` to signal a bad value.

This ACK Packet is sent regardless of whether or not the receiver actually
needed the block, as the original ACK packet may have been lost in transit and
the sender still needs validation.

When the sender receives an ACK packet, it ensures validity and removes the
block from the "still required" block queue so it is not retransmitted. As well
the block is removed from all outstanding bursts.

*Note*: One may note that this will cause bursts that contain the same block to
mistakenly believe they both received the same ACK. However, in practice this
occurs rarely due to the infrequent nature of packet loss and it most often
occurs at the very end of transmission, where the included blocks wrap within
each burst. As such, it has almost no impact on real-world performance.

At the sender's discretion, expired and satisfied bursts are cleaned from the
outstanding burst list and the information gained is used to update the RTT
and bandwidth estimates as defined in `Flow Control`__. It is recommended to run
such a cleanup procedure each timeout period as defined in `Round Trip Time
Estimation`__

Similarly, it is at the receiver's discretion to remove bursts from the offset
map.

Termination
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
When all blocks have been received, it's time for the connection to be
terminated. Since this data is not being streamed and we can assume common
knowledge of what packets are guaranteed to be received by the sender and which
are still needing to be acknowledged, it makes termination straight forward.
On arrival of the last ACK packet the sender publishes it's own acknowledge
packet with the max block ID + 1, a signal that it knows the file has been
completely transmitted. The receiver echos this acknowledge packet back to the
sender and immediately closes it's connection.

If either the sender or receiver acknowledge packets are lost, the sender will
attempt to retry after a short timeout period. If the maximum number of retries
fail, it timeouts and implicitly assumes that the receiver has closed and the
final ACK packet was simply lost.

However, if the receiver fails to receive the termination ACK from the sender,
it must assume that the sender has no knowledge of the success and, even if the
transfer was complete and successful, it will report a failure to the callsite.

Packet Types
--------------------------------------------------------------------------------
Packet headers are a fixed-size of 16 bytes. The first half consists of a 64-bit
(8-byte) common header, which includes universally required information such as
header kind, CRC error checking, and the multiplexing identifier. Following this
is a 64-bit (8-byte) specific header defined by the defined type of that packet.

*Note*: All fields labeled "padding" are required to be zero values.

The Base Header
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
The common header found among all others.

.. image:: figures/common-head.png


.. ┌────────┬─────────┬─────────┬──────────┐
   │ ver: 1 │ type: 3 │ crc: 16 │ uuid: 44 │
   └────────┴─────────┴─────────┴──────────┘
   ╾──────────────────64───────────────────╼

:CRC: CRC code for packet validation.
:UID: A unique identifier for multiplexing.
:Kind: The type of PROTO packet received.
:VER:
  A version tag - this specification only defines a value of `0` and leaves `1`
  to be used for future versions that may need to alter the header form in a
  not backwards-compatible way.

Connect Request
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
A header for connection requests (from sender).

.. image:: figures/creq-head.png

.. ┌───────────────┬───────────────┬─────────┐
   │ file_size: 48 │ block_size: 3 │ pad: 13 │
   └───────────────┴───────────────┴─────────┘
   ╾───────────────────64────────────────────╼

:Kind: CREQ (`000`)
:Filesize: The size, in bytes, of the file wanting to be sent.
:Block size:
  The value n for the equation :math:`2^{b+7}`, used to find our block width in
  bytes.
:Padding: Currently unused bits (alignment)

Request Augment
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
A second connection request header, bringing extra information (from sender) and
allowing for an initial bandwidth estimation to be produced.

.. image:: figures/raug-head.png

.. ┌──────────────┬─────────┐
   │ block_id: 32 │ pad: 32 │
   └──────────────┴─────────┘
   ╾───────────64───────────╼

:Kind: RAUG (`001`)
:Base Block: The first block to be used in data transfer.
:Padding: Currently unused bits (alignment)

*Note*: Though not in this specification, future versions of this specification
should go to define 16 bits of padding to represent the initial burst ID.

Connect Response
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
A header for connection responses (from receiver).

.. image:: figures/crep-head.png

.. ┌───────────┬───────────┬─────────┐
   │ ruuid: 44 │ refuse: 1 │ pad: 19 │
   └───────────┴───────────┴─────────┘
   ╾───────────────64───────────────╼

:Kind: CREP (`001`)
:RUID: A unique identifier for receiver-side multiplexing.
:Refused: Bit set only when the connection is refused by the receiver.
:Padding: Currently unused bits (alignment).

Data Block
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Data packets sent by the sender.

.. image:: figures/dat-head.png

.. ┌──────────────┬─────────┬──────────────────┐
   │ block_id: 32 │ pad: 32 │ data: block_size │
   └──────────────┴─────────┴──────────────────┘
   ╾────────────64──────────╼╾────block_size───╼

:Kind: DAT (`010`)
:Block ID: The sequence number tracking which block is currently being sent.
:Burst ID: Currently unused bits (alignment).

The data block is unique to all other packets in that directly proceding this
header follows :math:`2^{b+7}` bytes of data.

Acknowledge
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Acknowledgments of success made by the receiver.

.. image:: figures/ack-head.png

.. ┌──────────────┬───────────────┐
   │ block_id: 32 │ timeframe: 32 │
   └──────────────┴───────────────┘
   ╾──────────────64──────────────╼

:Kind: ACK (`011`)
:Block ID: The sequence number for which block was received.
:Timeframe: The number of nanoseconds from the first block of the corresponding
  data packet's burst.

*Note*: In such case that the timeframe cannot be determined or is out of range,
the value of `std::numeric_limit<uint32_t>::max()` should be provided to
indicate invalidity.

Wait
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
A packet notifying the sender or receiver that they are still alive, but are
unable to acknowledge or provide more data.

.. image:: figures/wait-head.png

.. ┌──────────────┐
   │ Padding: 64  │
   └──────────────┘
   ╾──────64──────╼

:Kind: WAIT (`100`)
:Padding: Currently unused bits (alignment).

Flow Control
--------------------------------------------------------------------------------

Round Trip Time Estimation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Having an estimated value of our round-trip time (RTT) and the deviation from
that average is quite useful in network transfers. This allows us to adjust our
timeouts to be as low as possible while also being long enough so as to not give
incoming packets a fair chance to arrive.

We begin with the initial RTT sample taken at handshake as the foundation. We
then perform basic exponential smoothing on our incoming samples to keep a
well-formed moving average of the RTT:

.. math:: RTT_e = \alpha * RTT_s + (1 - \alpha) * RTT_{e-1}

Along with this, we keep another moving average of the deviation from the
estimated RTT, with an initial value half of the RTT above:

.. math:: RTT_d = \beta * \mid RTT_s - RTT_e \mid + (1 - \beta) * RTT_{d-1}

And from these two factors, we may develop a useful timeout period for any
packet:

.. math:: T_t = 2^{f}(RTT_e) + n(RTT_d)

On may notice the distinct :math:`2^{f}` and `n` factors multiplying the
estimated RTT and deviation respectfully. The former is an exponential growth
factor by `f`, the number of consecutive timeouts experienced recently
experienced. Through this, we hope to allow a recovery from rapid and large
changes in the RTT that would otherwise cause an assumption of connection drop
(such as a mobile connection changing from wifi to a tower or even to a
satellite relay). The latter coefficient allows the system to prioritize waiting
a bit longer than the maximum expected RTT, just in case the next sample has a
higher deviation than currently averaged.

Bottleneck Bandwidth Estimation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Using the packets within a burst duration, we are able to generate a reasonably
accurate estimation of our network's bottleneck performance. To do this we
use knowledge of whether our first / last block in a burst were acknowledged,
and how many total ACKs were received relative to total transfer size.

.. image:: figures/bottleneck.png

The figure above, kindly provided by Usenix, visualizes how a network bottleneck
influences packets moving across the network. It goes such that networks sent
across have their distances increased. Moving further down the network, this
distance is not influenced unless another even tighter bottleneck is found.
This corresponds to the decrease in available bandwidth on the network.
By measuring the distance these packets come in on the receiver, it stands to
reason that we are truthfully measuring this bottleneck speed.

So, for each block that the sender has satisfied or expired, we run through the
equation:

.. math:: BW \approx \frac{(L_{block} + 16) * n}{t_H - t_L}

Essentially, the estimate of our network bandwidth is the number of bytes sent
over the amount of time it took to receive it. This estimate can be multiplied
by the time in each sender burst to determine how many blocks should be sent
for each burst.

**The Best Case - All ACKd**

.. image:: figures/all-good.png

As seen in the figure above, if all of our data packets are ACKd, we can make
a straightforward bandwidth estimate. The bandwidth estimation will be made over
the whole burst duration and thus will be completely representative.

**A Managable Case - First & Last ACKd**

.. image:: figures/inter-lost.png

Similarly, we can still make a reasonably accurate estimate in the case of lost
intermediate packets, so long as the first and last blocks are not lost. If we
were to calculate the overall bandwidth using only these timings, one will still
be determining bandwidth based on the overall area of burst duration.

**Worst Case - First || Last Lost**

.. image:: figures/m-lost.png

However, if the first or last block are lost as above, we are unable to reasonably
determine the bottleneck. The calculated bandwidth will be only between the
first packet actually received and the last block actually received.
There's no means to understand the distribution of the
ACKd packets in the burst and subsequently we may get a much higher value than
the network actually supports.

*Note*: One may also notice that this algorithm makes no attempt to account for
sender bottlenecking. However, as the sender has a defined burst duration and
will cut bursts short if that period is exceeded, it is a naturally understood
bottleneck.

Filtering
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
There are several cases in which the value of our bandwidth estimate has been
invalidated by conditions on the network. Particularly, if we make an estimate
generated on a burst where the first or last block was lost or if cross-traffic
causes the DAT packets to be placed directly next to each other in a queue after
the bottleneck point (where they will appear to the sender to be much closer in
proximity and thus higher bandwidth). As such, it is important to filter the
incoming estimates.

.. image:: figures/filter.png

The figure above, kindly provided by Usenix, show such exceptional cases (C-D)
compared to the base case (A). One may notice that the bandwidth estimate values
at A are grouped quite close together and thus follow the density of the system.

It is for this reason that we use a kernel density function to filter out
estimation samples that are sufficiently separated from our system (while not
tossing them out, as they may come to be the correct value in time).

.. math::

  D_{sample} = \frac{1}{n*h} \sum_{1}^{n} K(\frac{x-x_i}{h})

::

  Where
    D: Density of a sample
    n: Number of elements
    h: Smoothing (1.0 for reference)
    K(u): Kernel density function
    x: Sample point

The kernel function `K` provided must be a function whose value always falls
between 0 and 1. The Usenix report recommends using a triangular function
(which is quite efficient), but our reference chooses instead to use the
Guassian kernel:

.. math::

  K(u) = \frac{1}{\sqrt{2 \pi}} e^{-\frac{1}{2} u^2}

This has the benefit that the value of `u` has no bounds - we can use direct
sample measurements rather than having to make adjusted samples for the purpose
of sampling. It also provides a more normal distribution than triangular.

For the sake of simplicity, no other methods provided by the Usenix system are
required for bandwidth sample filtering.

The implementation is free to select it's own filtering algorithm as well as
supplemental mechanisms for bandwidth control.

Bandwidth Refinement via Lost Packets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
While these estimates are extremely useful and are often accurate "to a T" when
calculating the network bottleneck bandwidth over time, they are not agile
enough to account for sudden drops in network performance and can lead to
congestion when new connections contend for resources or the network introduces
a new bottleneck.

To counteract this, the specification defined a bandwidth modifier value (from
0.0 to 1.0) that is used as a coefficient with the bandwidth estimate to find
an adjusted network speed.

When a packet arrives containing the first and last data packets from the burst,
the bandwidth estimate is increased in a multiplicative linear way.

.. math::

  BW_e = BW_e = BW_p + (1 - BW_p) * \alpha

::

  Where:
    a is the coefficient of growth

This is very similar to the linear increase in TCP AIMD. However, rather than
linearly approaching the failure point again, it approaches it asymptotically
and extends the time before another reduction is made necessary.

Otherwise, if either of those mandatory packets are lost, the bandwidth estimate
is multiplicatively reduced:

.. math:

  M_bw = M_p * \beta

::

  Where:
    B is the shrink coefficient.

A small example script was made to show how these estimates work in practice.
Assuming a stable network speed with slight fluctuations, a network bandwidth
was simulated and fed into a kernel function which reacted to changes on the
network with a fallback AIMD modifier mode.

.. image:: figures/flow.png

The figure above shows the result of these tests. The estimate does well to find
the network bandwidth, but upon a sudden change does not have the agility to
react suddenly. For increases in the network bandwidth, this is more than
acceptable to take extra time to adjust. However, for losses this can be
catastrophic for fairness and performance.

But utilizing the fallback bandwidth modifier, the system can immediately react
to these losses and converge slowly and safely back to optimal.

Preventing Sender Network Saturation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A serious performance regression may occur in the case of a sudden drop in
network performance or an enigmatic overestimation of network capacity. In such
a case, the sender may oversaturate the network with DAT packets, preventing the
receiver the reprieve necessary to get out it's ACK packets. On the sender side,
this results in bloated RTT values and may even cause a burst to time out well
before any of the corresponding ACKs get back. Though one may argue that this
may be helpful to notify the sender to throttle back, it ultimately causes such
a dramatic change that overcorrection occurs or the result is filtered out by
the kernel smoothing.

Depending on the implementation, this problem can be made *significantly* worse.

If the receiver uses a single threaded DAT <-> ACK loop (as the reference
implementation does), the problem is made significantly worse. The receiver will
enter a halt state as the ACK attempts to send into a congested network, which
will disrupt the timings that do go out while causing unpredictable packet loss.

Even moreso if the receiver uses user-space time
measurements for the ACK's `ns_time` field. During this halted time, DAT packets
continue to stream in from the sender, filling the OS provided socket buffer.
When the send is finally pushed through, the receiver then rips through the
buffered packets and sends ACKs with timings not appropriate for the network's
bottleneck - but for the receiver's capabilities. If the sender has a
sufficiently high burst timeout, this may amplify the problem by causing the
sender to think the network is even faster than the current estimate and thus
send more packets (luckily, this is countered by our bandwidth modifier
adjusting on lost packets). If the sender has a burst timeout approximately
equal to the time it takes for these ACKs to get back, it can cause wild
fluctuations in the estimate that spoil the estimation dataset.

Unfortunately, there's little means to combat this problem without relying on a
mechanism we originally tried to avoid - the congestion window. Unlike with TCP,
the congestion window under PROTO designates the number of outstanding bursts
(not MSSs) that may be on the network at any given time. Starting at 1, this
number is incremented each time a burst expires with no packet loss (an
indication that the network is not congested at all) and the sender has skipped
bursts since the last congestion window update (signaling that the sender could
make use of a larger window). Then each time a burst expires with total loss,
it is decremented by one (but to no less than one).

For this latter aspect to function correctly (finding empty bursts), it is
generally recommended to follow the reference-advised burst timeout (see
`Round Trip Time Estimation`__).

This algorithm is quite unsuitable for a protocol such as TCP, it provides no
means of convergence. In fact, if we find that it
is prone to a persistant pendulum of overcorrection. However, one must remember
that this is designed as a supplemental mechanism to correct for bad behavior
that shouldn't often occur. Our primary means of bandwidth estimation are
themselves convergent and should, overtime, remove the activation of the
congestion window.

Summary of Flow Strategy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
In summary, we have:

* An estimate of the lowest point of bandwidth in the network used to calculate
  the number of packets in a send period.
* A means of filtering this estimate to account for the natural fluctuations in
  a network's available bandwidth.
* A modifier for this bandwidth to account for faulty estimates.
* A means to recover from sender saturation in the case of vastly overestimated
  durations.

With all of these means combined, we are able to accurately measure network
availability and converge to the optimal transmission speed as well as react
with decent agility toward sudden changes in network performance.

A Discussion of Fairness
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
It's difficult to guage the TCP fairness of an algorithm without considerable
analysis, but we will make an attempt to do so here. In brief, our algorithm
is somewhat TCP fair in theory (in that the mechanisms for control should cause
it to recognize and react to packet loss in a similar manner). However, in
practice we discover this to not be true.

Using Jain's fairness index on several test instances of resource contention
between PROTO and TCP, we find a value of approximately 0.61.

Reliability
--------------------------------------------------------------------------------

Corruption
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
While the UDP | IP standard we're transferring on does provide it's own
XOR-based 16-bit checksum, it's not quite enough for our needs. First of all,
UDP across IPv4 allows this checksum to go unused at the discretion of the host.
If the checksum is included, whether or not is particularly rigorous enough for
our purposes (many large packet sizes) is questionable.

Proto provides it's own CRC-CCITT validation for error detection in packets.

:Width: 16
:Generator Polynomial: 0x1021 (0x11021 implicit)
:Initial Value: 0xFFFF

The reference implementation uses a forward CRC table implementation.

In the table below, you'll find a relationship between the block_size field,
the corresponding packet byte size, the safe Hamming Distance (where no errors
will go undetected), and the Hamming Weight at the next distance. With this
information, one can determine the appropriate packet size to fit their needs in
terms of (header : data) size ratio, cost of packet loss, and the level of
acceptable errors.

==========  =========  ==  ========
block_size  byte_size  HD  HW(HD+1)
==========  =========  ==  ========
0           128        3   1,494,140
1           256        3   23,253,106
2           512        3   363,204,484
3           1024       3   5,766,916,951
4           2048       3   91,956,085,726
5           4096       1   17
6           8192       1   32,803
7           16384      1   196,682
==========  =========  ==  ========

We can develop a parameterised equation to find the probability of an
undetectable error occuring for some block size with this table.

.. math::

  P_u = \sum_{i}^{\infty} P_i * \frac{HW(i)}{\binom{L}{i}}

::

  Where:
    P_u: Probability of an undetected error
    P_i: (BER)^i * L
    HW:  Hamming Weight at i
    L:   Length of block in bits
    i: The generator's HD + 1 at this block size.
    BER: Bit Error Rate

This is the most accurate, but in general it isn't necessary. By nature of CRC
detection, for any odd value of i, we find the HW(i) to always be 0. And as bit
error rates tend to be within an order of magnitude of 10^{-5}, the next
non-zero value in the summation will be, at the least, about 8 orders of
magnitude lower than the previous. So we simplify the equation to something more
reasonable:

.. math::

  P_u = (BER)^i * L * \frac{HW(i)}{\binom{L}{i}}

::

  Where:
    P_u: Probability of an undetected error
    HW:  Hamming Weight at i
    L:   Length of block in bits
    BER: Bit Error Rate
    i: The generator's HD + 1 at this block size.

The selection of the block size is therefore a selection between the ability to
detect an error compared to the overhead of packet transmission. Though, in
general, the greatest overhead should only be on the order of +10% and the
highest block size will still allow for approximately 250 hours of 1 Gigabit
transmission without an undetected error.

It's always good and virtually free to run an MD5 checksum on the file after
transmission is complete, though, just to be sure of authenticity and
completeness.

Packet Loss
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The protocol is extremely resilient to packet loss. In the case that packet loss
occurs, the flow control mechanism help to swiftly reduce network pressure.
However, losing a packet does not require immediately resending and the protocol
moves on through the file to the end - wrapping around and coming back to
packets lost.

Unlike naive implementations of TCP, ordering does not matter and it never needs
to resend packets following a lost packet.

Packet Reordering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PROTO doesn't care about packet reordering - the order of blocks in a burst is
not important to the receiver, and the sender itself tracks first/last blocks
in a burst - never relying on the order they are received.

However, a connect attempt *will* be wasted if the CREQ / RAUG or CREP / CACK
pairs are mixed.

Packet Duplicates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The protocol as defined is naturally resilient to packet duplication. If a
duplicate packet comes to the receiver, it simply ACKs it back out. If the
sender encounters a duplicate, it simply ignores it as it will not show up in
the ACK gap list.

Reference Implementation
--------------------------------------------------------------------------------
In the sake of time and ease of implementation, the reference implementation is
actually *not* compliant with the specification.

No Multiplexing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The reference implementation is designed to meet the requirements of the
assignment given and does not venture into the complexity required for handling
multiple connections and multiplexing them as such. However, it does allow for
peer movement - so long as the GUID is the same and a packet can be received
with the new address before timeout.

Base Block Selection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Currently, the reference implementation selects a base block that will avoid
wrapping and subsequently requires a packet size appropriately large for any
sufficiently large filesize. As a result, the base block's "guessability" is
proportional to the size of your file and the block size selected. The receiver
is currently unable to cope with a sender that does not adhere to this
principle.

Base Burst Selection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The reference implementation uses an initial burst ID of 0, but the
specification prefers a random selection. This implementation choice was made
to greatly simplify the burst tracking such that the receive need only check
whether the burst ID is larger to know if it's newer. The receiver is able to
cope with deviating behavior, but wrapping the burst id will cause all future
blocks to come back with the invalid timing value.

Performance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The reference implementation does not go out of it's way to optimize. As such,
performance regressions can be seen when not enough CPU power is available to
the process.

Demonstration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To demonstrate the reference implementation, the provided testbed was used as a
receiver while our personal development machines were used as the senders. One
other test consisted of running both programs again on localhost. A test was run
with two PROTO streams sharing a network and one final test using PROTO on a
localhost with a simulated packet loss of 2%. All other network traffic was
disabled so as to not foul the results.

Our sent data consisted of a randomly selected 1.4GB file and was monitored for
the duration by Wireshark.

PROTO RemoteHost
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. image:: figures/proto-remote.png

2:53

Transmitting across a remote host, PROTO fairs well. It transmits at the highest
speed possible and has few fluctuations once it does converge to optimal.

PROTO RemoteHost 2% Loss
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. image:: figures/proto-remote-loss.png

3:15

Under the influence of packet loss, it fluctuates much more due to the nature of
our flow control mechanisms. However, it still manages to stay fairly consistent
and loses transmission speed only slightly more than proportional to the
increase in loss rate.

PROTO LocalHost
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. image:: figures/proto-local.png

1:10

On localhost, though, PROTO has more trouble finding an optimal speed. The RTT
is so low, the bandwidth so high, and it all varies largely on CPU / disk speed,
so it's difficult for it to find a convergence.

TCP RemoteHost
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. image:: figures/tcp-remote.png

6:50

As expected, TCP is extremely stable and converges very nicely to the optimal
bandwidth. However, the nature of TCP's operation causes it to be over twice
as slow as PROTO is.

TCP LocalHost
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. image:: figures/tcp-local.png

1:26

As one can see, these issues are not constrained to PROTO. Even TCP has a bit of
trouble finding the convergence point.

TCP Fairness Test
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. image:: figures/proto-tcp-fairness.png

:Green: Overall Network
:Red: PROTO Stream
:Black: TCP Stream

As seen above, the network resources clearly aren't being shared fairly.
However, it's interesting to note that the TCP connection maintains a decent
proportion of it's original value - implying that PROTO isn't quite TCP fair,
but also isn't being an aggregious hog either.

PROTO Fairness Test
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
.. image:: figures/proto-fairness.png

:Black: Overall Network
:Green: PROTO Stream 1
:Red: PROTO Stream 2

From the results above, it seems clear that PROTO demonstrates self-fairness and
will attempt to equally share resources when two such streams conflict.

Citations
--------------------------------------------------------------------------------

A Quantitative Measure of Fairness ... for Resource Allocation ...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:Purpose: A description of Jain's fairness index used to determine the fairness
of our system.
:Author: Jain, Ciue, & Hawe
:Publisher: Eastern Research Lab

Bittorent Specification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:Purpose: Torrents focus on piecemeal file transfer, attempting to be TCP fair
  and have a valid, complete file after transfer. While not much was taken from
  the specification below, the original idea for the block transfer system came
  from such torrenting systems.
:URL: http://www.bittorrent.org/beps/bep_0003.html

CRC Zoo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:Purpose: Information on Hamming Distance and Weights at specific data lengths
  was kindly calculated and provided by the CRC "Zoo".
:URL: https://users.ece.cmu.edu/~koopman/crc/c16/0x8810.txt

A Painless Guide to CRC Error Detection Algorithms
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:Purpose: Helped solidify an understanding of how CRC actually works and the
  means by which one could compute them for a given dataset.
:URL: http://www.ross.net/crc/download/crc_v3.txt

CRC-16-CCITT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:Purpose: Provided the CCITT generator table used in our codebase.
:URL: http://automationwiki.com/index.php/CRC-16-CCITT

Packet Pair Technique
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:Purpose: In the development of the Nettimer tool, this source provides a
  sufficient summary of the packet pair bandwidth estimation technique. More
  importantly, and it's primary use in our project, was providing extremely
  useful algorithms for packet pair sample filters - to reduce unwanted noise
  in our measurements.
:URL: https://goo.gl/MK7Aom

Exponential Smoothing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:Purpose: Detailed explanation of the exponential smoothing function used for
  our estimation round-trip time and deviation averages.
:URL: https://en.wikipedia.org/wiki/Exponential_smoothing

Computer Networking: A Top Down Approach (7th ed.)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
:Purpose: A fantastic book that provides the bulk of our learned information
  about networking - in specific and as a whole.
:Author: Kurose & Ross
:Publisher: Pearson
