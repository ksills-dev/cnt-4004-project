build-dir := build/

#------------------------------------------------------------------------------
# Helpers
#------------------------------------------------------------------------------

define dir_guard =
	$(shell \
		if [ ! -d $(dir $1) ]; then \
			if [ $(platform) == "Windows_NT" ]; then \
				mkdir $(dir $1); \
			else \
				mkdir -p $(dir $1); \
			fi \
		fi\
	)
endef

#------------------------------------------------------------------------------
# Targets
#------------------------------------------------------------------------------

.PHONY: all clean
.DEFAULT: all

all:
	$(call dir_guard,$(build-dir))
	cd build && cmake .. && make

clean:
	rm -r $(build-dir)