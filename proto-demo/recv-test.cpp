#include <proto/receive.hpp>

#ifndef NDEBUG
#include <glog/logging.h>
#endif

#include <iostream>
#include <limits>
#include <string>
#include <vector>

auto main(int argc, char const **argv) -> int {
#ifndef NDEBUG
  google::InitGoogleLogging(argv[0]);

  FLAGS_log_dir = "logs/";
#endif

  std::vector<std::string> pretty_argv{};
  pretty_argv.reserve(static_cast<size_t>(argc) - 1);
  for (auto i = 1; i < argc; i++) {
    pretty_argv.emplace_back(argv[i]);
  }

  if (argc < 2) {
    std::cout << "Missing port number!\n";
    return -1;
  }
  unsigned long port;
  try {
    port = std::stoul(pretty_argv[0]);
  } catch (std::invalid_argument &) {
    std::cout << "Invalid port number given: " << pretty_argv[0] << std::endl;
    return -1;
  } catch (std::out_of_range &) {
    std::cout << "Port number out of range: " << pretty_argv[0] << std::endl;
    return -1;
  }

  if (port < 1024 || port > std::numeric_limits<uint16_t>::max()) {
    std::cout << "Port number out of range: " << pretty_argv[0] << std::endl;
    return -1;
  }

  std::string filename = "out.dat";
  if (argc > 2) {
    filename = pretty_argv[1];
  }

  float loss = 0.0;
  if (argc > 3) {
    try {
      loss = std::stof(pretty_argv[2]);
    } catch (std::invalid_argument &) {
      std::cout << "Invalid loss given: " << pretty_argv[2] << std::endl;
      return -1;
    } catch (std::out_of_range &) {
      std::cout << "Loss out of range: " << pretty_argv[2] << std::endl;
      return -1;
    }
    if (loss < float(0.0) || loss > float(100.0)) {
      std::cout << "Loss out of valid 0-100 range: " << loss << std::endl;
      return -1;
    }
  }

  std::cout << "\033[1m[Starting File Receiver]...\n\033[0m"
            << "Port: " << port << std::endl
            << "File: " << filename << std::endl
            << "Loss Rate: " << loss * 100 << "%\n\n";

  auto result = proto::receive_file(
      filename, std::numeric_limits<size_t>::max(), static_cast<uint16_t>(port),
      proto::Receive_Params{}.loss(loss));
  std::cout << std::endl;
  if (result == proto::Receive_Result::Success) {
    std::cout << "\033[32mSuccessful.\n\033[0m";
    return 0;
  } else {
    std::cout << "\033[31Failed\n\033[0m";
    return -1;
  }
}
