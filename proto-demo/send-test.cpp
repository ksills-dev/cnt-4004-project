#include <proto/send.hpp>

#include <chrono>
#include <iostream>
#include <string>
#include <vector>

#ifndef NDEBUG
#include <glog/logging.h>
#endif

auto main(int argc, char const **argv) -> int {
#ifndef NDEBUG
  google::InitGoogleLogging(argv[0]);
  FLAGS_log_dir = "logs/";
#endif

  std::vector<std::string> pretty_argv{};
  pretty_argv.reserve(argc - 1);
  for (auto i = 1; i < argc; i++) {
    pretty_argv.emplace_back(argv[i]);
  }

  if (argc < 2) {
    std::cout << "Missing My Port.";
    return -1;
  }
  unsigned long my_port;
  try {
    my_port = std::stoul(pretty_argv[0]);
  } catch (std::invalid_argument &) {
    std::cout << "Invalid port number given: " << pretty_argv[0] << std::endl;
    return -1;
  } catch (std::out_of_range &) {
    std::cout << "My port number out of range: " << pretty_argv[0] << std::endl;
    return -1;
  }

  if (my_port < 1024 || my_port > std::numeric_limits<uint16_t>::max()) {
    std::cout << "My port number out of range: " << my_port << std::endl;
    return -1;
  }

  if (argc < 3) {
    std::cout << "Missing Peer IP.";
    return -1;
  }
  std::string peer_ip = pretty_argv[1];

  if (argc < 4) {
    std::cout << "Missing Peer Port.";
    return -1;
  }
  unsigned long peer_port;
  try {
    peer_port = std::stoul(pretty_argv[2]);
  } catch (std::invalid_argument &) {
    std::cout << "Invalid peer port number given: " << pretty_argv[2]
              << std::endl;
    return -1;
  } catch (std::out_of_range &) {
    std::cout << "Peer port number out of range: " << pretty_argv[2]
              << std::endl;
    return -1;
  }

  if (peer_port < 1024 || peer_port > std::numeric_limits<uint16_t>::max()) {
    std::cout << "Peer port number out of range: " << peer_port << std::endl;
    return -1;
  }

  std::string filename = "in.dat";
  if (argc > 4) {
    filename = pretty_argv[3];
  }

  float loss = 0.0;
  if (argc > 5) {
    try {
      loss = std::stof(pretty_argv[4]);
    } catch (std::invalid_argument &) {
      std::cout << "Invalid loss given: " << pretty_argv[4] << std::endl;
      return -1;
    } catch (std::out_of_range &) {
      std::cout << "Loss out of range: " << pretty_argv[4] << std::endl;
      return -1;
    }
    if (loss < float(0.0) || loss > float(100.0)) {
      std::cout << "Loss out of valid 0-100 range: " << pretty_argv[4]
                << std::endl;
      return -1;
    }
  }

  std::cout << "\033[1m[Starting File Sender]\033[0m\n"
            << "Filename: " << filename << std::endl
            << "Peer IP: " << peer_ip << std::endl
            << "Peer Port: " << peer_port << std::endl
            << "My Port: " << my_port << std::endl
            << "Loss: " << loss * 100 << "%\n\n";

  auto result = proto::send_file(filename, static_cast<uint16_t>(my_port),
                                 static_cast<uint16_t>(peer_port), peer_ip,
                                 proto::Send_Params{}.loss(loss));
  std::cout << std::endl;
  if (result == proto::Send_Result::Success) {
    std::cout << "\033[32mSuccessful\n\033[0m";
  } else {
    std::cout << "\033[31mFailed\n\033[0m";
  }
}
