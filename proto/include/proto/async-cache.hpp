#pragma once

#include "packet.hpp"

#include <semaphore.h>

#include <atomic>
#include <cstdint>
#include <memory>

namespace proto {

/**
Asynchronous Caching object, allowing one to read and write blocks of
memory safely across threads.

@note Only thread safe for consumer/producer pairs. Does not protect against
consumer/consumer or producer/producer conflicts.
*/
class Async_Cache {
public:
  Async_Cache() = delete; // Can't default construct.

  /**
  Constructs a new cache, allocating the buffer and preparing the
  synchronization mechanisms.

  @throws std::system_error With the appropriate errno if a semaphore operation
  fails.
  */
  Async_Cache(size_t size, size_t block_size);

  // No assigning or moving a cache.
  Async_Cache(const Async_Cache &original) = delete;
  Async_Cache(Async_Cache &&original) = delete;
  auto operator=(const Async_Cache &original) -> Async_Cache & = delete;
  auto operator=(Async_Cache &&original) -> Async_Cache & = delete;

  /**
  Destructor for cache. Frees all contained resources.
  */
  ~Async_Cache();

  /**
  Write a single block to the buffer and notify any readers. If no space is
  available to write into, this function will block.

  @throws std::system_error With the appropriate errno if a semaphore operation
  fails.

  @see try_write_block
  */
  auto write_block(uint8_t const *data) -> void;

  /**
  A non-blocking form of `write_block`. Returns true on successful write, false
  otherwise.

  @throws std::system_error With the appropriate errno if a semaphore operation
  fails.

  @see write_block
  */
  auto try_write_block(uint8_t const *data) -> bool;

  /**
  Read a single block from the buffer into the target and notify any writers.
  If no blocks are available to read, this function will block.

  @throws std::system_error With the appropriate errno if a semaphore operation
  fails.

  @see try_read_block
  */
  auto read_block(uint8_t *target) -> void;

  /**
  A non-blocking form of `read_block`. Returns true on successful read, false
  otherwise.

  @throws std::system_error With the appropriate errno if a semaphore operation
  fails.

  @see read_block
  */
  auto try_read_block(uint8_t *target) -> bool;

  /**
  Don't use this without notifying the callsite to die, as they will likely
  consume bad data!
  */
  auto unsafe_notify() -> void;

private:
  std::atomic_size_t _read_head;  ///< Index of next read point.
  std::atomic_size_t _write_head; ///< Index of next write point.
  sem_t *_read_sync;  ///< Semaphore to halt reads until data is available.
  sem_t *_write_sync; ///< Semaphore to halt reads until space in the buffer is
                      ///< available.

  size_t _block_size;               ///< Size of each partition in the buffer.
  size_t _size;                     ///< Number of blocks in the buffer.
  std::unique_ptr<uint8_t> _buffer; ///< Data buffer.
};

} // namespace proto
