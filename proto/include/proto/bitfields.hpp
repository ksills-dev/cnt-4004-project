#pragma once

#include <cmath>
#include <string>
#include <type_traits>

namespace proto {
namespace util {

template <class T, size_t bitwidth, size_t offset>
auto pack_bitfield(T original) -> T {
  static_assert(std::is_scalar<T>::value,
                "Can only pack bitfields of scalar types.");
  static_assert(bitwidth <= (sizeof(T) * 8),
                "Bitfield must not exceed the size of T.");
  static_assert(bitwidth > 0, "Bitfield must have a non-zero size.");
  static_assert(offset < sizeof(T) * 8,
                "Offset must be less than the size of T.");
  return ((original) & ((T{1} << bitwidth) - 1)) << offset;
}

template <class T, size_t bitwidth, size_t offset>
auto unpack_bitfield(T original) -> T {
  static_assert(std::is_scalar<T>::value,
                "Can only unpack bitfields of scalar types.");
  static_assert(bitwidth <= (sizeof(T) * 8),
                "Bitfield must not exceed the size of T.");
  static_assert(bitwidth > 0, "Bitfield must have a non-zero size.");
  static_assert(offset < sizeof(T) * 8,
                "Offset must be less than the size of T.");
  return ((original >> offset) & ((T{1} << bitwidth) - 1));
}

template <class T> auto to_little_endian(T original) -> T {
  static_assert(std::is_scalar<T>::value,
                "Can only convert endianness of scalar types.");
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
  return original;
#else
  T result = 0;
  for (auto i = 0; i < sizeof(T); i++) {
    result = result << 8;
    result |= original & 0xFF;
    original = original >> 8;
  }
  return result;
#endif
}

template <class T> auto from_little_endian(T original) -> T {
  static_assert(std::is_scalar<T>::value,
                "Can only convert endianness of scalar types.");
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
  return original;
#else
  static_assert(false, "This shouldn't happen");
  T result = 0;
  for (auto i = 0; i < sizeof(T); i++) {
    result = result << 8;
    result |= original & 0xFF;
    original = original >> 8;
  }
  return result;
#endif
}

} // namespace util
} // namespace proto
