#pragma once

#include <chrono>
#include <cmath>
#include <vector>

namespace proto {
namespace flow {
/*------------------------------------------------------------------------------
RTT Estimation
--------------------------------------------------------------------------------
RTT maintained and modified as a float, converted to some integer granularity
for use in timer.
*/

using RTT = std::chrono::nanoseconds;

static double const default_rtte_coeff = 0.125;
static double const default_rttd_coeff = 0.25;

auto rtt_step(RTT sample, RTT &estimate, RTT &deviation,
              float e_coeff = default_rtte_coeff,
              float d_coeff = default_rttd_coeff) -> void;

/*------------------------------------------------------------------------------
Bandwidth Estimation
--------------------------------------------------------------------------------
A sample is in the form of blocks per millisecond for ease of use at burst prep.
(bytes / block_size) / (ns_time / 1,000,000)

To use, multiply bandwidth sample by frequency of burst to get the number of
blocks per burst. Use the largest between the ceiling of this value or 2.
*/

static size_t const default_kernel_set_size = 128;
static float const default_kernel_smoothing = 1.0;

auto gauss_kernel(float t) -> float;

template <float(K)(float t) = gauss_kernel> class BW_Estimator {
public:
  BW_Estimator() = delete;
  BW_Estimator(float initial_sample, size_t set_size = default_kernel_set_size,
               float kernel_smoothing = default_kernel_smoothing)
      : kd_smoothing(kernel_smoothing), _oldest(0) {
    _current_estimate = initial_sample;
    _sample_set.reserve(set_size);
    _sample_set.push_back(initial_sample);
  }

  ~BW_Estimator() = default;
  BW_Estimator(BW_Estimator const &original) = default;
  auto operator=(BW_Estimator const &original) -> BW_Estimator & = default;

  auto refine(float new_sample) -> float {
    if (_sample_set.size() == _sample_set.capacity()) {
      _sample_set[_oldest++] = new_sample;
      _oldest %= _sample_set.size();
    } else {
      _sample_set.push_back(new_sample);
    }

    auto best_fitness = 0.0f;
    auto best_estimate = 0.0f;
    for (auto const &sample : _sample_set) {
      auto fitness = kernel_density(sample);
      if (fitness > best_fitness) {
        best_fitness = fitness;
        best_estimate = sample;
      }
    }
    _current_estimate = best_estimate;
    return best_estimate;
  }

  auto estimate() const -> float { return _current_estimate; }

private:
  auto kernel_density(float sample) -> float {
    const auto height = kd_smoothing * _sample_set.size();
    float result = 0.0;
    for (auto i = 0; i < height; i++) {
      result += K((sample - _sample_set[i]) / (kd_smoothing * sample));
    }
    result *= (1.0 / height);
    return result;
  }

  float kd_smoothing;

  float _current_estimate;
  size_t _oldest;
  std::vector<float> _sample_set;
};

/*------------------------------------------------------------------------------
Bandwidth Modifier
--------------------------------------------------------------------------------
*/

static float const default_mod_growth = 0.1f;
static float const default_mod_shrink = 0.9f;
auto bw_mod_inc(float mod, float growth_factor = default_mod_growth) -> float;
auto bw_mod_dec(float mod, float shrink_factor = default_mod_shrink) -> float;

} // namespace flow

} // namespace proto
