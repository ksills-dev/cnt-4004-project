#pragma once

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <string>

namespace proto {

enum class Socket_Address_Type { Ip4, Ip6 };

class Socket_Address {
public:
  Socket_Address() = default;

  /**
  Implicit construction from an initialized POSIX sockaddr.
  */
  Socket_Address(sockaddr const *addr);

  /**
  Construction given a specific port and a string representation of an IP
  address. Whether the socket will be IPv6 or not will be determined based on
  the form of the provided string.

  @throws std::invalid_exception When the provided string does not match any
  correct IP address form.
  */
  Socket_Address(unsigned short port, std::string const &addr);

  /**
  Constructor given a specified port over any IP address. Provided address type
  used for the socket family.
  */
  Socket_Address(Socket_Address_Type type, unsigned short port);

  /// Default copy constructor.
  Socket_Address(const Socket_Address &original) = default;
  /// Default move constructor.
  Socket_Address(Socket_Address &&original) = default;

  /// Default destructor.
  ~Socket_Address() = default;

  /// Default copy assignment.
  auto operator=(const Socket_Address &original) -> Socket_Address & = default;
  /// Default move assignment.
  auto operator=(Socket_Address &&original) -> Socket_Address & = default;

  auto ip_ver() const -> Socket_Address_Type;
  auto get_posix_addr() const -> sockaddr *;
  auto len() const -> socklen_t;

  auto family() const -> int;

  auto port() const -> unsigned short;
  auto port(unsigned short port) -> Socket_Address &;
  auto addr() const -> std::string;
  auto addr(std::string ip) -> Socket_Address &;

protected:
  friend class Packet_Socket;

  Socket_Address_Type _kind;
  union InternalSockAddrVariant {
    sockaddr_in ip4_dat;
    sockaddr_in6 ip6_dat;
  };
  InternalSockAddrVariant _dat;
  socklen_t _len = sizeof(sockaddr_in6);

private:
};

} // namespace proto
