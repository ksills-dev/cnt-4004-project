#pragma once

#include <cstdint>
#include <mutex>
#include <vector>

namespace proto {

struct Partition {
  uint32_t first;
  size_t length;
};

class Partition_List {
public:
  Partition_List() = delete;
  Partition_List(uint32_t first, size_t size);

  Partition_List(const Partition_List &original) = default;
  Partition_List(Partition_List &&original) = default;

  ~Partition_List() = default;

  auto operator=(const Partition_List &original) -> Partition_List & = default;
  auto operator=(Partition_List &&original) -> Partition_List & = default;

  auto needs(uint32_t id) -> bool;
  auto satisfy(uint32_t id) -> bool;
  auto next() -> uint32_t;
  auto empty() -> bool;

private:
  uint32_t _head;
  size_t _head_loc;
  std::vector<Partition> _partitions;
  std::recursive_mutex _access_lock;
};

} // namespace proto
