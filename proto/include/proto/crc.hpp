#pragma once

#include <cstdint>
#include <cstring>

namespace proto {
namespace crc {

using CRC16 = uint16_t;

auto generate_crc(uint8_t const *data, size_t size) -> uint16_t;
auto validate_crc(uint8_t const *data, size_t size, uint16_t crc) -> bool;

} // namespace crc
} // namespace proto
