#pragma once

#include "flow-ctrl.hpp"

#include <chrono>
#include <cstdint>
#include <deque>
#include <string>

namespace proto {

struct Send_Params {
public:
  Send_Params();

  Send_Params(Send_Params const &original) = default;
  Send_Params(Send_Params &&original) = default;

  auto operator=(Send_Params const &original) -> Send_Params & = default;
  auto operator=(Send_Params &&original) -> Send_Params & = default;

  ~Send_Params() = default;

  auto cache_size() const -> size_t { return _cache_size; }
  auto block_size() const -> size_t { return _block_size; }
  auto max_wait_string() const -> size_t { return _max_wait_string; }
  auto max_connect_attempts() const -> size_t { return _max_connect_attempts; }
  auto max_transfer_attempts() const -> size_t {
    return _max_transfer_attempts;
  }
  auto max_term_attempts() const -> size_t { return _max_term_attempts; }
  auto connect_timeout() const -> std::chrono::milliseconds {
    return _connect_timeout;
  }
  auto transfer_timeout() const -> std::chrono::milliseconds {
    return _transfer_timeout;
  }
  auto burst_length() const -> std::chrono::milliseconds {
    return _burst_length;
  }
  auto initial_rtte() const -> flow::RTT { return _initial_rtte; }
  auto initial_rttd() const -> flow::RTT { return _initial_rttd; }
  auto loss() const -> float { return _loss; }

  auto cache_size(size_t value) -> Send_Params & {
    _cache_size = value;
    return *this;
  }
  auto block_size(size_t value) -> Send_Params & {
    _block_size = value;
    return *this;
  }
  auto max_wait_string(size_t value) -> Send_Params & {
    _max_wait_string = value;
    return *this;
  }
  auto max_connect_attempts(size_t value) -> Send_Params & {
    _max_connect_attempts = value;
    return *this;
  }
  auto max_transfer_attempts(size_t value) -> Send_Params & {
    _max_transfer_attempts = value;
    return *this;
  }
  auto max_term_attempts(size_t value) -> Send_Params & {
    _max_term_attempts = value;
    return *this;
  }
  auto connect_timeout(std::chrono::milliseconds value) -> Send_Params & {
    _connect_timeout = value;
    return *this;
  }
  auto transfer_timeout(std::chrono::milliseconds value) -> Send_Params & {
    _transfer_timeout = value;
    return *this;
  }
  auto burst_length(std::chrono::milliseconds value) -> Send_Params & {
    _burst_length = value;
    return *this;
  }
  auto initial_rtte(flow::RTT value) -> Send_Params & {
    _initial_rtte = value;
    return *this;
  }
  auto initial_rttd(flow::RTT value) -> Send_Params & {
    _initial_rttd = value;
    return *this;
  }
  auto loss(float value) -> Send_Params & {
    _loss = value;
    return *this;
  }

private:
  float _loss;
  size_t _cache_size;
  size_t _block_size;
  size_t _max_wait_string;
  size_t _max_connect_attempts;
  size_t _max_transfer_attempts;
  size_t _max_term_attempts;
  std::chrono::milliseconds _connect_timeout;
  std::chrono::milliseconds _transfer_timeout;
  std::chrono::milliseconds _burst_length;
  flow::RTT _initial_rtte;
  flow::RTT _initial_rttd;
};

enum class Send_Result {
  Success,
  Bad_Address,
  Connect_Timeout,
  Connect_Exceeded_Attempts,
  Connection_Refused,
  Transfer_Timeout,
  Transfer_Exceeded_Waits,
  System_Error
};

auto send_file(std::string const &filename, uint16_t my_port,
               uint16_t peer_port, std::string const &receiver_ip,
               Send_Params const &options = Send_Params{}) -> Send_Result;

} // namespace proto
