#pragma once

#ifdef __clang__
#define TRUST_OLD_CAST(code)                                                   \
  _Pragma("clang diagnostic push")                                             \
      _Pragma("clang diagnostic ignored \"-Wold-style-cast\"")                 \
          code _Pragma("clang diagnostic pop")
#elif defined __GNUC__
#define TRUST_OLD_CAST(code)                                                   \
  _Pragma("GCC diagnostic push")                                               \
      _Pragma("GCC diagnostic ignored \"-Wold-style-cast\"")                   \
          code _Pragma("GCC diagnostic pop")
#else
#endif
