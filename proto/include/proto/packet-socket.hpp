#pragma once

#include "maybe.hpp"
#include "packet.hpp"
#include "socket-address.hpp"

#include <poll.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <bitset>
#include <cerrno>
#include <chrono>
#include <cstdint>
#include <exception>
#include <memory>
#include <system_error>

namespace proto {

enum Expected_Packet {
  CREQ = 1,
  RAUG = 2,
  CREP = 4,
  DAT = 8,
  ACK = 16,
  WAIT = 32
};

/**

*/
class Packet_Socket {
public:
  Packet_Socket() = default; // No default construction.
  Packet_Socket(Socket_Address const &from);

  Packet_Socket(const Packet_Socket &original) = default;
  Packet_Socket(Packet_Socket &&original) = default;

  auto operator=(const Packet_Socket &original) -> Packet_Socket & = default;
  auto operator=(Packet_Socket &&original) -> Packet_Socket & = default;

  ~Packet_Socket();

  auto rebind(Socket_Address const &from) -> bool;

  auto fetch(Packet &target, std::bitset<8> expected,
             std::chrono::milliseconds timeout) -> util::Maybe<Socket_Address>;
  auto fetch(Packet &target, std::bitset<8> expected,
             std::chrono::milliseconds timeout, Socket_Address &from) -> bool;

  auto send(Packet &packet, Socket_Address target) -> void;

private:
  int _socket_handle;
  Socket_Address _my_addr;
};

} // namespace proto
