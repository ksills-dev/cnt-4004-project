#pragma once

#include <exception>
#include <string>
#include <type_traits>

// Task List:
// Testing
namespace proto {
namespace util {

/**
 * @brief Small exception type for invalid access to a Maybe type with no valid
 * state.
 */
class InvalidMaybeAccess : std::exception {
public:
  /**
   * @brief Basic default constructor.
   */
  InvalidMaybeAccess(){};

  /**
   * @brief Return a C-string describing our exceptional case.
   */
  virtual auto what() const noexcept -> const char * {
    return "Cannot access empty Maybe type.";
  };
};

/**
 * @brief Very small, generic class to represent an object that may or may not
 * exist.
 *
 * # Using Maybe<T>
 * Expected is really designed to be constructed from it's builder functions,
 * then manipulated through the contained methods.
 *
 * Some returns a full and valid Maybe type from a given value, while Some
 * returns
 * an empty Maybe type (often to designate failure).
 *
 * ```
 * auto test(bool condition) -> Maybe<int>
 * {
 *     if (condition)
 *         return Some(5);
 *     else
 *         return None<int>();
 * }
 *
 * auto pass = test(true);
 * auto fail = test(false);
 *
 * assert(pass.exists());
 * assert(fail.empty())
 * ```
 *
 * # Contraints
 * It's implicitly assumed that the class specializing Maybe<T> implements
 * a copy constructor. If not, you'll get a nice compile-time error.
 * As well, failure to provide (or default) a valid destructor will lead
 * to a similar compile time error.
 *
 * @see Some
 * @see None
 * @see Expected
 */
template <typename T> class Maybe {
  static_assert(std::is_copy_constructible<T>::value,
                "Classes specializing Maybe<T> must implement (or default) a "
                "copy constructor.");
  static_assert(std::is_copy_assignable<T>::value,
                "Classes specializing Maybe<T> must implement (or default) a "
                "copy assignment operator.");
  static_assert(std::is_destructible<T>::value, "Classes specializing Maybe<T> "
                                                "must implement (or default) a "
                                                "destructor.");

public:
  using ValueType = T;

  ~Maybe() {
    if (_exists) {
      _value.~T();
    }
  }

  /**
   * @brief Basic copy constructor.
   */
  Maybe(const Maybe &original) : _exists(original._exists) {
    // Using new we generate an object at our specific address using the
    // desired copy constructor.
    if (_exists) {
      new (&_value) T(original._value);
    }
  }

  /**
   * @brief Basic move constructor.
   */
  Maybe(Maybe &&original) : _exists(original._exists) {
    // Using new we generate an object at our specific address using the
    // desired copy constructor.
    if (_exists) {
      new (&_value) T(std::move(original._value));
    }
  }

  /**
   * @brief Basic assignment operator.
   */
  auto operator=(const Maybe &original) -> Maybe & {
    if (original._exists) {
      if (_exists) {
        _value = original._value;
      } else {
        new (&_value) T(original._value);
      }
    } else {
      if (_exists) {
        _value.~T();
      }
    }
    _exists = original._exists;
    return *this;
  }

  /**
   * @brief Basic move assignment operator.
   */
  auto operator=(Maybe &&original) -> Maybe & {
    if (original._exists) {
      if (_exists) {
        _value = original._value;
      } else {
        new (&_value) T(std::move(original._value));
      }
    } else {
      if (_exists) {
        _value.~T();
      }
    }
    _exists = original._exists;
    return *this;
  }

  /**
   * @brief Returns true if the Maybe type is full and contains a value.
   * @see empty
   */
  auto exists() const -> bool { return _exists; }

  /**
   * @brief Returns true if the Maybe type is empty and does not contain a
   * value.
   * @see exists
   */
  auto empty() const -> bool { return !_exists; }

  /**
   * @brief Return our contained value.
   * @throws InvalidMaybeAccess when no value exists.
   * @see unwrap
   */
  auto access() const -> T {
    if (_exists) {
      return _value;
    } else {
      throw InvalidMaybeAccess();
    }
  }

  /**
   * @brief Return our contained value.
   * @throws InvalidMaybeAccess when no value exists.
   * @see access
   */
  auto unwrap() const -> T {
    if (_exists) {
      return _value;
    } else {
      throw InvalidMaybeAccess();
    }
  }

private:
  Maybe() : _exists(false){}; // Default exists to false, just in case.

  // Friend builder function forward declaration.
  template <typename T_> friend auto Some(const T_ &value) -> Maybe<T_>;
  template <typename T_> friend auto Some(T_ &&value) -> Maybe<T_>;
  template <typename T_> friend auto None() -> Maybe<T_>;

  bool _exists; ///< Whether or not our value is valid and usable.
  union {
    T _value;
  }; ///< The internal value of our Maybe type.
     // Unionized as to prevent initialization.
};

/**
 * @brief Constructs a full Maybe<T> object from a copy of the given value.
 *
 * @see None
 */
template <typename T> auto Some(const T &value) -> Maybe<T> {
  auto result = Maybe<T>();
  result._exists = true;
  new (&result._value) T(value);
  return result;
}

/**
 * @brief Constructs a full Maybe<T> object, moved from the given value.
 *
 * @see None
 */
template <typename T> auto Some(T &&value) -> Maybe<T> {
  auto result = Maybe<T>();
  result._exists = true;
  new (&result._value) T(std::move(value));
  return result;
}

/**
 * @brief Returns an empty Maybe<T> object.
 *
 * @see Some
 */
template <typename T> auto None() -> Maybe<T> {
  auto result = Maybe<T>();
  result._exists = false;
  return result;
}
} // namespace util
} // namespace proto
