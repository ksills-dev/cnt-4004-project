#pragma once

#include "crc.hpp"

#include <cstdint>
#include <vector>

namespace proto {

using UID = uint64_t;

enum class Packet_Type {
  CREQ = 0,
  RAUG = 1,
  CREP = 2,
  DAT = 3,
  ACK = 4,
  WAIT = 5,
  PAD = 6,
  PAD2 = 7
};

struct Base_Packet_Header {
  crc::CRC16 crc;
  UID uid;
  Packet_Type kind;
  bool version;

  auto pack() const -> uint64_t;
  static auto unpack(uint64_t buffered) -> Base_Packet_Header;
};

struct CREQ_Packet_Header {
  uint64_t file_size;
  uint8_t block_size;
  uint16_t pad;

  auto pack() const -> uint64_t;
  static auto unpack(uint64_t buffered) -> CREQ_Packet_Header;
};

struct RAUG_Packet_Header {
  uint32_t base_block;
  uint32_t pad;

  auto pack() const -> uint64_t;
  static auto unpack(uint64_t buffered) -> RAUG_Packet_Header;
};

struct CREP_Packet_Header {
  UID ruid;
  bool refuse;
  uint32_t pad;

  auto pack() const -> uint64_t;
  static auto unpack(uint64_t buffered) -> CREP_Packet_Header;
};

struct DAT_Packet_Header {
  uint16_t pad;
  uint16_t burst_id;
  uint32_t block_id;

  auto pack() const -> uint64_t;
  static auto unpack(uint64_t buffered) -> DAT_Packet_Header;
};

struct ACK_Packet_Header {
  uint32_t block_id;
  uint32_t ns_time;

  auto pack() const -> uint64_t;
  static auto unpack(uint64_t buffered) -> ACK_Packet_Header;
};

struct WAIT_Packet_Header {
  uint64_t pad;

  auto pack() const -> uint64_t;
  static auto unpack(uint64_t buffered) -> WAIT_Packet_Header;
};

/**

## Sending / Receiving
packet.[un]pack();
packet.validate() / packet.generate_crc();
packet.send_ptr();

## Fetching Block
When read/writing data to the Async_Cache, one should use the block_ptr. This
does not return the address of the data, but 4-bytes prior to it as to include
the block_id.

*/
class Packet {
public:
  Packet();
  Packet(size_t block_size);

  Packet(const Packet &original) = default;
  Packet(Packet &&original) = default;

  ~Packet() = default;

  auto operator=(const Packet &original) -> Packet & = default;
  auto operator=(Packet &&original) -> Packet & = default;

  auto clear() -> void;
  auto resize(size_t block_size) -> void;

  auto buffer_size() const -> size_t;
  auto buffer() -> uint8_t *;

  auto block_size() const -> size_t;
  auto block_ptr() -> uint8_t *;

  auto load_creq(UID uid, size_t file_size, uint8_t block_size) -> void;
  auto load_raug(UID uid, uint32_t base_block) -> void;
  auto load_crep(UID uid, UID ruid, bool refuse) -> void;
  auto load_dat(UID uid, uint16_t burst_id) -> void;
  auto load_ack(UID uid, uint32_t block_id, uint32_t ns_time) -> void;
  auto load_wait(UID uid) -> void;

  Base_Packet_Header base;
  union {
    CREQ_Packet_Header creq_dat;
    RAUG_Packet_Header raug_dat;
    CREP_Packet_Header crep_dat;
    DAT_Packet_Header dat_dat;
    ACK_Packet_Header ack_dat;
    WAIT_Packet_Header wait_dat;
  };

protected:
  friend class Packet_Socket;

  auto validate() -> bool;
  auto generate_crc() -> void;

  auto pack() -> void;
  auto unpack() -> void;

  bool _ready;
  bool _packed;

private:
  std::vector<uint8_t> _buffer;
};

auto CREQ_Packet(UID uid, size_t file_size, uint8_t block_size) -> Packet;
auto RAUG_Packet(UID uid, uint32_t base_block) -> Packet;
auto CREP_Packet(UID uid, UID ruid, bool refuse) -> Packet;
auto DAT_Packet(UID uid, uint16_t burst_id, size_t block_size) -> Packet;
auto ACK_Packet(UID uid, uint32_t block_id, uint32_t ns_time) -> Packet;
auto WAIT_Packet(UID uid) -> Packet;

} // namespace proto
