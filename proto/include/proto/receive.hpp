#pragma once

#include "async-cache.hpp"
#include "maybe.hpp"
#include "packet-socket.hpp"
#include "packet.hpp"
#include "partition.hpp"
#include "socket-address.hpp"

#include <chrono>
#include <cstdint>
#include <string>

namespace proto {

enum class Receive_Result {
  Success,
  Connect_Timeout,
  Connect_Exceeded_Attempts,
  Bad_Connect,
  Transfer_Timeout,
  Transfer_Exceeded_Attempts,
  Transfer_Exceeded_Waits,
  System_Error
};

struct Receive_Params {
public:
  Receive_Params();

  Receive_Params(Receive_Params const &original) = default;
  Receive_Params(Receive_Params &&original) = default;
  auto operator=(Receive_Params const &original) -> Receive_Params & = default;
  auto operator=(Receive_Params &&original) -> Receive_Params & = default;
  ~Receive_Params() = default;

  auto cache_size() const -> size_t { return _cache_size; }
  auto max_connect_attempts() const -> size_t { return _max_connect_attempts; }
  auto max_transfer_attempts() const -> size_t {
    return _max_transfer_attempts;
  }
  auto max_wait_string() const -> size_t { return _max_wait_string; }
  auto connect_timeout() const -> std::chrono::milliseconds {
    return _connect_timeout;
  }
  auto transfer_timeout() const -> std::chrono::milliseconds {
    return _transfer_timeout;
  }
  auto burst_track_time() const -> std::chrono::milliseconds {
    return _burst_track_time;
  }
  auto loss() const -> float { return _loss; }

  auto cache_size(size_t const value) -> Receive_Params & {
    _cache_size = value;
    return *this;
  }
  auto max_connect_attempts(size_t const value) -> Receive_Params & {
    _max_connect_attempts = value;
    return *this;
  }
  auto max_transfer_attempts(size_t const value) -> Receive_Params & {
    _max_transfer_attempts = value;
    return *this;
  }
  auto max_wait_string(size_t const value) -> Receive_Params & {
    _max_wait_string = value;
    return *this;
  }
  auto connect_timeout(std::chrono::milliseconds const value)
      -> Receive_Params & {
    _connect_timeout = value;
    return *this;
  }
  auto transfer_timeout(std::chrono::milliseconds const value)
      -> Receive_Params & {
    _transfer_timeout = value;
    return *this;
  }
  auto burst_track_time(std::chrono::milliseconds const value)
      -> Receive_Params & {
    _burst_track_time = value;
    return *this;
  }
  auto loss(float value) -> Receive_Params & {
    _loss = value;
    return *this;
  }

private:
  float _loss;
  size_t _cache_size;
  size_t _max_connect_attempts;
  size_t _max_transfer_attempts;
  size_t _max_wait_string;
  std::chrono::milliseconds _connect_timeout;
  std::chrono::milliseconds _transfer_timeout;
  std::chrono::milliseconds _burst_track_time;
};

auto receive_file(std::string const &filename, size_t max_filesize,
                  uint16_t port,
                  Receive_Params const &options = Receive_Params{})
    -> Receive_Result;

} // namespace proto
