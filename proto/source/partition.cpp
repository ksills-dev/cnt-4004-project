#include "proto/partition.hpp"

namespace proto {

Partition_List::Partition_List(uint32_t first, size_t size)
    : _head(first), _head_loc(0), _partitions() {
  _partitions.push_back(Partition{first, size});
}

auto Partition_List::needs(uint32_t id) -> bool {
  std::lock_guard<std::recursive_mutex> lock(_access_lock);
  if (empty()) {
    return false;
  }

  size_t i = 0;
  size_t j = _partitions.size();
  if (id == _head) {
    return true;
  }
  if (id > _head) {
    i = _head_loc;
  } else {
    j = _head_loc + 1;
  }

  for (; i < j; i++) {
    auto partition = _partitions[i];
    if (id < partition.first) {
      return false;
    } else if (id >= partition.first + partition.length) {
      continue;
    } else {
      return true;
    }
  }
  return false;
}

auto Partition_List::satisfy(uint32_t id) -> bool {
  std::lock_guard<std::recursive_mutex> lock(_access_lock);
  size_t i = 0;
  size_t j = _partitions.size();
  if (id == _head) {
    i = _head_loc;
    j = _head_loc + 1;
    this->next();
  } else if (id > _head) {
    i = _head_loc;
  } else {
    j = _head_loc + 1;
  }

  for (; i < j; i++) {
    auto partition = _partitions[i];
    if (id < partition.first) {
      return false;
    } else if (id >= partition.first + partition.length) {
      continue;
    }

    if (partition.first == id) {
      if (partition.length == 1) {
        _partitions.erase(_partitions.begin() +
                          std::vector<Partition>::difference_type(i));
        if (_head_loc > i) {
          _head_loc--;
        }
      } else {
        _partitions[i].first++;
        _partitions[i].length--;
      }
      return true;
    } else if (partition.first + partition.length - 1 == id) {
      _partitions[i].length--;
      return true;
    } else if (partition.first < id &&
               id < partition.first + partition.length) {
      _partitions[i].length = id - partition.first;
      _partitions.insert(
          _partitions.begin() + std::vector<Partition>::difference_type(i) + 1,
          Partition{id + 1, partition.first + partition.length - id - 1});
      if (_head > id) {
        _head_loc++;
      }
      return true;
    }
  }
  return false;
}

auto Partition_List::next() -> uint32_t {
  std::lock_guard<std::recursive_mutex> lock(_access_lock);
  if (_partitions.empty()) {
    _head = std::numeric_limits<uint32_t>::max();
    _head_loc = 0;
    return _head;
  }

  uint32_t result = _head++;
  auto head_partition = _partitions[_head_loc];
  if (_head >= head_partition.first + head_partition.length) {
    _head_loc = (_head_loc + 1) % _partitions.size();
    _head = _partitions[_head_loc].first;
  }
  return result;
}

auto Partition_List::empty() -> bool {
  std::lock_guard<std::recursive_mutex> lock{_access_lock};
  return _partitions.empty();
}

} // namespace proto
