#include "proto/socket-address.hpp"
#include "proto/misc.hpp"

#include <regex>
#include <stdexcept>

#include <arpa/inet.h>

namespace proto {

// ([0-9]?[0-9]|[0-1]?[0-9][0-9]|2[0-4][0-9]|25[0-5])
static std::regex const ipv4_regex{"()/.{3}()"};

Socket_Address::Socket_Address(sockaddr const *addr) {
  switch (addr->sa_family) {
  case AF_INET:
    _kind = Socket_Address_Type::Ip4;
    _dat.ip4_dat = *reinterpret_cast<sockaddr_in const *>(addr);
    break;
  case AF_INET6:
    _kind = Socket_Address_Type::Ip6;
    _dat.ip6_dat = *reinterpret_cast<sockaddr_in6 const *>(addr);
    break;
  default:
    throw std::invalid_argument(
        "Socket_Address created with invalid sockaddr type.");
  }
}

Socket_Address::Socket_Address(unsigned short port, std::string const &addr) {
  int result = -1;
  if (addr.find(':') == std::string::npos) {
    _kind = Socket_Address_Type::Ip4;
    _dat.ip4_dat.sin_family = AF_INET;
    _dat.ip4_dat.sin_port = htons(port);
    result = inet_pton(AF_INET, addr.c_str(), &_dat.ip4_dat.sin_addr);
  } else {
    _kind = Socket_Address_Type::Ip6;
    _dat.ip6_dat.sin6_family = AF_INET6;
    _dat.ip6_dat.sin6_port = htons(port);
    result = inet_pton(AF_INET6, addr.c_str(), &_dat.ip6_dat.sin6_addr);
  }
  if (result == 0) {
    throw std::invalid_argument(
        "Socket_Address::Socket_Address() provided invalid IP address.");
  }
  if (result == -1) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
}

Socket_Address::Socket_Address(Socket_Address_Type type, unsigned short port) {
  _kind = type;
  switch (_kind) {
  case Socket_Address_Type::Ip4:
    _dat.ip4_dat.sin_family = AF_INET;
    _dat.ip4_dat.sin_port = htons(port);
    TRUST_OLD_CAST(
        _dat.ip4_dat.sin_addr = in_addr{htonl(
            INADDR_ANY)}; // TODO: Find a better way to suppress warning.
    )
    break;
  case Socket_Address_Type::Ip6:
    _dat.ip6_dat.sin6_family = AF_INET6;
    _dat.ip6_dat.sin6_port = htons(port);
    _dat.ip6_dat.sin6_addr = in6addr_any;
    break;
  }
}

auto Socket_Address::ip_ver() const -> Socket_Address_Type { return _kind; }

auto Socket_Address::get_posix_addr() const -> sockaddr * {
  // WARNING:
  // FIXME:
  // BUG:
  // TODO: This calls attention to a lack of const-correctness on this class!
  return reinterpret_cast<sockaddr *>(
      const_cast<Socket_Address::InternalSockAddrVariant *>(&_dat));
}

auto Socket_Address::len() const -> socklen_t { return _len; }

auto Socket_Address::family() const -> int {
  switch (_kind) {
  case Socket_Address_Type::Ip4:
    return _dat.ip4_dat.sin_family;
  case Socket_Address_Type::Ip6:
    return _dat.ip6_dat.sin6_family;
  }
  return 0;
}

auto Socket_Address::port() const -> unsigned short {
  switch (_kind) {
  case Socket_Address_Type::Ip4:
    return ntohs(_dat.ip4_dat.sin_port);
  case Socket_Address_Type::Ip6:
    return ntohs(_dat.ip6_dat.sin6_port);
  }
  return 0;
}

auto Socket_Address::port(unsigned short port) -> Socket_Address & {
  switch (_kind) {
  case Socket_Address_Type::Ip4:
    _dat.ip4_dat.sin_port = htons(port);
    break;
  case Socket_Address_Type::Ip6:
    _dat.ip6_dat.sin6_port = htons(port);
    break;
  }
  return *this;
}

auto Socket_Address::addr() const -> std::string {
  switch (_kind) {
  case Socket_Address_Type::Ip4: {
    std::string result{INET_ADDRSTRLEN};
    inet_ntop(AF_INET, &_dat.ip4_dat, const_cast<char *>(result.data()), _len);
    return result;
  }
  case Socket_Address_Type::Ip6: {
    std::string result{INET6_ADDRSTRLEN};
    inet_ntop(AF_INET, &_dat.ip6_dat, const_cast<char *>(result.data()), _len);
    return result;
  }
  }
  return "FATAL_ERR";
}

auto Socket_Address::addr(std::string ip) -> Socket_Address & {
  *this = Socket_Address{port(), ip};
  return *this;
}

} // namespace proto
