#include "proto/receive.hpp"

#include "proto/async-cache.hpp"
#include "proto/bitfields.hpp"
#include "proto/maybe.hpp"
#include "proto/packet-socket.hpp"
#include "proto/packet.hpp"
#include "proto/socket-address.hpp"

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <thread>
#include <unordered_map>

#ifndef NDEBUG
#include <glog/logging.h>
#endif

namespace proto {

namespace {

static float const default_loss_rate = 0.0;
static size_t const default_cache_size = 1024;
static size_t const default_max_connect_attempts = 3;
static size_t const default_max_transfer_attempts = 3;
static size_t const default_max_wait_string = 100;
static std::chrono::milliseconds const default_connect_timeout{3000};
static std::chrono::milliseconds const default_transfer_timeout{3000};
static std::chrono::milliseconds const default_burst_track_time{20000};

auto cache_job(std::FILE *file, uint64_t file_size, uint32_t initial_block,
               uint32_t n_blocks, size_t block_size, Async_Cache &cache,
               std::atomic_bool const &kill_flag, Partition_List &gap_list)
    -> void {
#ifndef NDEBUG
  DLOG(INFO) << "Receiver caching job started.";
#endif

  std::vector<uint8_t> buffer{};
  buffer.reserve(4 + block_size);
  auto last_block = initial_block + n_blocks - 1;
  auto last_size = file_size - (n_blocks - 1) * block_size;
  std::uint32_t previous_block = std::numeric_limits<uint32_t>::max();

  while (true) {
    if (gap_list.empty()) {
      if (!cache.try_read_block(buffer.data())) {
        break;
      }
    } else {
      cache.read_block(buffer.data());
    }
    if (kill_flag) {
      break;
    }

    auto block_id =
        util::from_little_endian(*reinterpret_cast<uint32_t *>(buffer.data()));
    size_t write_size = (block_id == last_block) ? last_size : block_size;
    if (previous_block != (block_id - 1)) {
      long file_index =
          static_cast<long>((block_id - initial_block) * block_size);
      std::fseek(file, file_index, SEEK_SET);
    }
    previous_block = block_id;
    std::fwrite(buffer.data() + 4, 1, write_size, file);
  }
#ifndef NDEBUG
  DLOG(INFO) << "Cache job complete, closing file...";
#endif
  fclose(file);
}

} // namespace

Receive_Params::Receive_Params()
    : _loss(default_loss_rate), _cache_size(default_cache_size),
      _max_connect_attempts(default_max_connect_attempts),
      _max_transfer_attempts(default_max_transfer_attempts),
      _max_wait_string(default_max_wait_string),
      _connect_timeout(default_connect_timeout),
      _transfer_timeout(default_transfer_timeout),
      _burst_track_time(default_burst_track_time) {}

auto receive_file(std::string const &filename, size_t max_filesize,
                  uint16_t port, Receive_Params const &options)
    -> Receive_Result {

  Receive_Result result = Receive_Result::Success;

  Packet creq_packet{};
  Packet raug_packet{};
  Packet_Socket socket{Socket_Address{Socket_Address_Type::Ip4, port}};

// Connect Handshake
#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "Receiver starting handshake phase...";

  auto expire_time =
      std::chrono::high_resolution_clock::now() + options.connect_timeout();

  std::chrono::time_point<std::chrono::high_resolution_clock> creq_time;
  std::chrono::time_point<std::chrono::high_resolution_clock> raug_time;
  Socket_Address peer{};
  size_t attempts = 0;
  while (true) {
    if (std::chrono::high_resolution_clock::now() >= expire_time) {
#ifndef NDEBUG
      DLOG(WARNING) << "Connection phase has timed out.";
#endif
      return Receive_Result::Connect_Timeout;
    }
    if (attempts >= options.max_connect_attempts()) {
#ifndef NDEBUG
      DLOG(WARNING)
#else
      std::cout
#endif
          << "Handshake has exceeded max number of attempts.";
      return Receive_Result::Connect_Exceeded_Attempts;
    }

    auto source = socket.fetch(
        creq_packet, CREQ,
        std::chrono::duration_cast<std::chrono::milliseconds>(
            expire_time - std::chrono::high_resolution_clock::now()));
    creq_time = std::chrono::high_resolution_clock::now();
    if (!source.exists()) {
      expire_time =
          std::chrono::high_resolution_clock::now() + options.connect_timeout();
#ifndef NDEBUG
      DLOG(INFO) << "No Valid CREQ Found.";
#endif
      attempts += 1;
      continue;
    }
#ifndef NDEBUG
    DLOG(INFO) << "Valid CREQ Found, searching for accompanying RAUG...";
#endif

    source = socket.fetch(
        raug_packet, RAUG,
        std::chrono::duration_cast<std::chrono::milliseconds>(
            expire_time - std::chrono::high_resolution_clock::now()));
    raug_time = std::chrono::high_resolution_clock::now();
    if (!source.exists() || raug_packet.base.uid != creq_packet.base.uid) {
#ifndef NDEBUG
      DLOG(WARNING) << "Valid RAUG Not Acquired...";
#endif
      expire_time =
          std::chrono::high_resolution_clock::now() + options.connect_timeout();
      attempts += 1;
      continue;
    }
    peer = source.unwrap();

#ifndef NDEBUG
    DLOG(INFO) << "Valid RAUG Found.";
#endif
    break;
  }

#ifndef NDEBUG
  DLOG(INFO) << "Establishing Connection Resources...";
#endif

  // WARNING: DO NOT USE THIS IN PRODUCTION CODE!
  std::srand(static_cast<unsigned int>(
      std::chrono::high_resolution_clock::now().time_since_epoch().count()));

  uint64_t const suid = creq_packet.base.uid;
  uint64_t const ruid = uint64_t(std::rand()) % (uint64_t(1) << 44);
  uint64_t const file_size = creq_packet.creq_dat.file_size;
  uint16_t const block_size =
      static_cast<uint16_t>(1 << (7 + creq_packet.creq_dat.block_size));
  uint32_t const initial_block = raug_packet.raug_dat.base_block + 1;
  uint32_t const n_blocks = static_cast<uint32_t>(
      (file_size / block_size) + ((file_size % block_size) ? 1 : 0));
  bool refuse = false;

#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "[Connection Parameters]"               //
      << "\n\tSUID: " << suid                    //
      << "\n\tRUID: " << ruid                    //
      << "\n\tFilesize: " << file_size           //
      << "\n\tBlock size: " << block_size        //
      << "\n\tBase block: " << initial_block - 1 //
      << "\n\t# Blocks: " << n_blocks << "\n";

  auto loss = int(100 * options.loss());
  int dropped = 0;

  if (file_size > max_filesize) {
#ifndef NDEBUG
    DLOG(WARNING)
#else
    std::cout
#endif
        << "Requested filesize exceeds user limit.";
    refuse = true;
    result = Receive_Result::Bad_Connect;
  }
  if (((file_size / uint64_t(block_size)) + ((file_size % block_size) ? 1 : 0) >
       n_blocks)) {
#ifndef NDEBUG
    DLOG(WARNING)
#else
    std::cout
#endif
        << "Given filesize is too large to safely meet blocksize "
           "requirement.";
    refuse = true;
    result = Receive_Result::Bad_Connect;
  }
  if (uint64_t(initial_block) + uint64_t(n_blocks) >=
      std::numeric_limits<uint32_t>::max()) {
#ifndef NDEBUG
    DLOG(WARNING)
#else
    std::cout
#endif
        << "Blocks will wrap before completion.";
    refuse = true;
    result = Receive_Result::Bad_Connect;
  }

  std::atomic_bool kill_flag{false};
  Async_Cache cache{options.cache_size(),
                    size_t((block_size + 4))}; // +4 to account for block_id.
  auto file = std::fopen(filename.c_str(), "w");
  if (file == nullptr) {
#ifndef NDEBUG
    DLOG(WARNING)
#else
    std::cout
#endif
        << "Could not open file for writing.";
    refuse = true;
    result = Receive_Result::System_Error;
  }

  Partition_List gap_list{initial_block, n_blocks};
  std::unordered_map<
      uint32_t, std::chrono::time_point<std::chrono::high_resolution_clock>>
      burst_offsets{};
  auto newest_burst = 0;

  std::thread cache_thread{
      cache_job,         file,       file_size,       initial_block,
      n_blocks,          block_size, std::ref(cache), std::ref(kill_flag),
      std::ref(gap_list)};

  auto last_wait = std::chrono::high_resolution_clock::now();

#ifndef NDEBUG
  DLOG(INFO) << "Sending response packets.";
#endif

  auto crep_packet = CREP_Packet(suid, ruid, refuse);
  auto cack_packet =
      ACK_Packet(suid, initial_block - 1,
                 static_cast<uint32_t>(
                     std::chrono::duration_cast<std::chrono::nanoseconds>(
                         raug_time - creq_time)
                         .count()));
  socket.send(crep_packet, peer);
  socket.send(cack_packet, peer);

  if (refuse) {
    return result;
  }

// Transfer Phase
#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "Starting Transfer Phase...\n";

  attempts = 0;
  size_t wait_string = 0;
  bool term_ack = false;
  Packet response_packet{};
  Packet transfer_packet{block_size};
  expire_time =
      std::chrono::high_resolution_clock::now() + options.transfer_timeout();
  while (!term_ack) {
    if (std::chrono::high_resolution_clock::now() >= expire_time) {
#ifndef NDEBUG
      DLOG(WARNING) << "Transfer phase has timed out.";
#endif
      kill_flag = true;
      cache.unsafe_notify();
      cache_thread.join();
      return Receive_Result::Transfer_Timeout;
    }
    if (attempts >= options.max_transfer_attempts()) {
#ifndef NDEBUG
      DLOG(WARNING)
#else
      std::cout
#endif
          << "Exceeded maximum transfer attempts.";
      kill_flag = true;
      cache.unsafe_notify();
      cache_thread.join();
      return Receive_Result::Transfer_Exceeded_Attempts;
    }
    if (wait_string >= options.max_wait_string()) {
#ifndef NDEBUG
      DLOG(WARNING)
#else
      std::cout
#endif
          << "Exceeded maximum wait string.";
      kill_flag = true;
      cache.unsafe_notify();
      cache_thread.join();
      return Receive_Result::Transfer_Exceeded_Waits;
    }

    auto from = socket.fetch(
        transfer_packet, CREQ | DAT | ACK | WAIT,
        std::chrono::duration_cast<std::chrono::milliseconds>(
            expire_time - std::chrono::high_resolution_clock::now()));
    if (!from.exists()) {
#ifndef NDEBUG
      DLOG(WARNING) << "Valid transfer packet not found.";
#endif
      expire_time = std::chrono::high_resolution_clock::now() +
                    options.transfer_timeout();
      attempts += 1;
      continue;
    }
    peer = from.unwrap();

    if (std::rand() % 100 < loss) {
      // Simulate packet loss
      dropped++;
      continue;
    }

    switch (transfer_packet.base.kind) {
    case Packet_Type::CREQ:
      if (transfer_packet.base.uid != suid) {
#ifndef NDEBUG
        DLOG(INFO) << "Ignoring new CREQ packet.";
#endif
        break;
      }
#ifndef NDEBUG
      DLOG(INFO) << "CREQ duplicate found, retransmitting acceptance.";
#endif
      expire_time = std::chrono::high_resolution_clock::now() +
                    options.transfer_timeout();
      attempts = 0;
      wait_string = 0;
      socket.send(crep_packet, peer);
      socket.send(cack_packet, peer);
      break;

    case Packet_Type::DAT: {
      if (transfer_packet.base.uid != ruid) {
#ifndef NDEBUG
        DLOG(INFO) << "Ignoring irrelevant DAT packet.";
#endif
        break;
      }
      attempts = 0;
      wait_string = 0;
      if (transfer_packet.dat_dat.block_id < initial_block ||
          transfer_packet.dat_dat.block_id >= initial_block + n_blocks) {
#ifndef NDEBUG
        DLOG(WARNING) << "Ignoring block outside of valid id range.";
#endif
        break;
      }

      expire_time = std::chrono::high_resolution_clock::now() +
                    options.transfer_timeout();
      auto recv_time = std::chrono::high_resolution_clock::now();

      std::vector<uint16_t> marked{};
      for (auto &pair : burst_offsets) {
        if (recv_time - pair.second > options.burst_track_time()) {
#ifndef NDEBUG
          DLOG(INFO) << "Cleaning expired burst info for " << pair.first;
#endif
          marked.push_back(static_cast<uint16_t>(pair.first));
        }
      }
      for (auto const &mark : marked) {
        burst_offsets.erase(mark);
      }

      uint32_t offset_recv_time;
      if (burst_offsets.count(transfer_packet.dat_dat.burst_id)) {
        offset_recv_time = static_cast<uint32_t>(
            std::chrono::duration_cast<std::chrono::nanoseconds>(
                recv_time - burst_offsets[transfer_packet.dat_dat.burst_id])
                .count());
      } else {
        if (transfer_packet.dat_dat.burst_id < newest_burst) {
          offset_recv_time = std::numeric_limits<uint32_t>::max();
        } else {
#ifndef NDEBUG
          DLOG(INFO) << "Packet indicates new burst "
                     << transfer_packet.dat_dat.burst_id;
#endif
          newest_burst = transfer_packet.dat_dat.burst_id;
          burst_offsets[transfer_packet.dat_dat.burst_id] = recv_time;
          offset_recv_time = 0;
        }
      }

      auto block_id = transfer_packet.dat_dat.block_id;
      bool send_ack = false;
      bool send_wait = false;

      if (gap_list.needs(block_id)) {
        if (!cache.try_write_block(transfer_packet.block_ptr())) {
          send_ack = false;
          if ((std::chrono::high_resolution_clock::now() >=
               last_wait + std::chrono::milliseconds{10})) {
            last_wait = std::chrono::high_resolution_clock::now();
#ifndef NDEBUG
            DLOG(WARNING) << "Waiting on cache job - sending WAIT packet.";
#endif
            send_wait = true;
          }
        } else {
          if (gap_list.satisfy(block_id)) {
            send_ack = true;
            if (gap_list.empty()) {
#ifndef NDEBUG
              DLOG(INFO)
#else
              std::cout
#endif
                  << "All blocks have been satisfied.\n";
            }
          }
        }
      } else {
        send_ack = true;
      }

      if (send_ack) {
        response_packet.load_ack(suid, transfer_packet.dat_dat.block_id,
                                 offset_recv_time);
        socket.send(response_packet, from.access());
      }
      if (send_wait) {
        response_packet.load_wait(suid);
        socket.send(response_packet, from.access());
      }
      break;
    }

    case Packet_Type::ACK:
      if (transfer_packet.base.uid != ruid) {
#ifndef NDEBUG
        DLOG(INFO) << "Ignoring irrelevant ACK packet.";
#endif
      }
      wait_string = 0;
      attempts = 0;
      if (not gap_list.empty()) {
#ifndef NDEBUG
        DLOG(WARNING) << "TACK Packet received, but still missing blocks!";
#endif
      } else if (transfer_packet.ack_dat.block_id == initial_block + n_blocks) {
#ifndef NDEBUG
        DLOG(INFO)
#else
        std::cout
#endif
            << "Found valid sender TACK.\n";
        term_ack = true;
      } else {
#ifndef NDEBUG
        DLOG(WARNING) << "Ignoring ACK Packet for "
                      << transfer_packet.ack_dat.block_id;
#endif
      }
      break;
    case Packet_Type::WAIT:
      if (transfer_packet.base.uid != ruid) {
#ifndef NDEBUG
        DLOG(INFO) << "Ignoring irrelevant WAIT packet.";
#endif
        break;
      }
#ifndef NDEBUG
      DLOG(INFO) << "Found a wait packet. Resetting transfer timeout.";
#endif
      expire_time = std::chrono::high_resolution_clock::now() +
                    options.transfer_timeout();
      wait_string += 1;
      attempts = 0;
      break;

    case Packet_Type::CREP: // Falthrough
    case Packet_Type::RAUG: // Falthrough
    case Packet_Type::PAD:  // Falthrough
    case Packet_Type::PAD2: // Falthrough
#ifndef NDEBUG
      DLOG(FATAL) << "This is quite literally impossible";
#endif
    }
  }

// Termination Phase
#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "Transfer phase ended, moving to termination.\n";

  Packet tack_packet{};
  tack_packet.load_ack(suid, initial_block + n_blocks,
                       std::numeric_limits<uint32_t>::max());

#ifndef NDEBUG
  DLOG(INFO) << "Sending TACK packet";
#endif
  socket.send(tack_packet, peer);

// Cleanup
#ifndef NDEBUG
  DLOG(INFO) << "Connection terminated, cleaning up and returning.";
#endif

#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "Receiver dropped " << dropped << " packets.\n";
  cache.unsafe_notify();
  cache_thread.join();

  return Receive_Result::Success;
}

} // namespace proto
