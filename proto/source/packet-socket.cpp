#include "proto/packet-socket.hpp"

#include <poll.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <cerrno>
#include <chrono>
#include <cstdint>
#include <system_error>
#include <vector>

namespace proto {

Packet_Socket::Packet_Socket(Socket_Address const &self) : _my_addr(self) {
  _socket_handle = socket(self.family(), SOCK_DGRAM, 0);
  if (_socket_handle == -1 ||
      bind(_socket_handle, self.get_posix_addr(), self.len())) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
}

Packet_Socket::~Packet_Socket() {
  if (_socket_handle != -1) {
    close(_socket_handle);
  }
}

auto Packet_Socket::rebind(Socket_Address const &self) -> bool {
  _my_addr = self;
  socket(self.family(), SOCK_DGRAM, 0);
  if (_socket_handle == -1 ||
      bind(_socket_handle, self.get_posix_addr(), self.len()) != 0) {
    return false;
  }
  return true;
}

auto Packet_Socket::fetch(Packet &target, std::bitset<8> expected,
                          std::chrono::milliseconds timeout)
    -> util::Maybe<Socket_Address> {
  sockaddr_in6 from{};
  socklen_t from_len = sizeof(from);

  auto expire_time = std::chrono::high_resolution_clock::now() + timeout;

  do {
    // Wait for an event
    auto poll_target = pollfd{_socket_handle, POLLIN, 0};
    auto waiting =
        poll(&poll_target, 1,
             static_cast<int>(
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     expire_time - std::chrono::high_resolution_clock::now())
                     .count()));
    if (waiting == -1) {
      throw std::system_error(std::error_code{errno, std::system_category()});
    } else if (waiting == 1) {
      target._packed = false;
      // Fetch packet
      auto received = recvfrom(_socket_handle, target.buffer(),
                               target.buffer_size(), MSG_DONTWAIT,
                               reinterpret_cast<sockaddr *>(&from), &from_len);
      if (received == -1) {
        throw std::system_error(std::error_code{errno, std::system_category()});
      } else if (received != 16 && received != int(target.buffer_size())) {
        // This can't be a block, it's neither a header nor data block.
      } else {
        // Unpack, validate, and make sure it's an expected packet - else we
        // toss it.
        target.unpack();
        if (target.validate()) {
          if (expected[static_cast<uint8_t>(target.base.kind)]) {
            return util::Some(
                Socket_Address{reinterpret_cast<sockaddr *>(&from)});
          }
        }
      }
    }
  } while (std::chrono::high_resolution_clock::now() < expire_time);
  return util::None<Socket_Address>();
}

auto Packet_Socket::fetch(Packet &target, std::bitset<8> expected,
                          std::chrono::milliseconds timeout,
                          Socket_Address &from) -> bool {
  auto result = fetch(target, expected, timeout);
  if (result.exists()) {
    from = result.access();
    return true;
  }
  return false;
}

auto Packet_Socket::send(Packet &packet, Socket_Address target) -> void {
  packet.pack();
  packet.generate_crc();
  if (sendto(_socket_handle, packet.buffer(), packet.buffer_size(), 0,
             target.get_posix_addr(), target._len) == -1) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
}

} // namespace proto
