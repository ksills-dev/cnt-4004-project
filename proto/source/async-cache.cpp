#include "proto/async-cache.hpp"

#include <cstdio>
#include <cstring>
#include <system_error>

namespace proto {

Async_Cache::Async_Cache(size_t size, size_t block_size)
    : _read_head(0), _write_head(0), _block_size(block_size), _size(size),
      _buffer(new uint8_t[size * block_size]) {
  _read_sync = static_cast<sem_t *>(calloc(1, sizeof(sem_t)));
  _write_sync = static_cast<sem_t *>(calloc(1, sizeof(sem_t)));
  if (sem_init(_read_sync, 0, 0) == -1 ||
      sem_init(_write_sync, 0, static_cast<unsigned int>(size)) == -1) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
}

Async_Cache::~Async_Cache() {
  sem_destroy(_read_sync);
  sem_destroy(_write_sync);
  free(_read_sync);
  free(_write_sync);
}

auto Async_Cache::write_block(uint8_t const *data) -> void {
  if (sem_wait(_write_sync)) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
  std::memcpy(_buffer.get() + (_write_head++ % _size) * _block_size, data,
              _block_size);
  if (sem_post(_read_sync)) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
}

auto Async_Cache::read_block(uint8_t *target) -> void {
  if (sem_wait(_read_sync)) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
  std::memcpy(target, _buffer.get() + (_read_head++ % _size) * _block_size,
              _block_size);
  if (sem_post(_write_sync)) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
}

auto Async_Cache::try_write_block(uint8_t const *data) -> bool {
  int sem_result;
  if (sem_getvalue(_write_sync, &sem_result)) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
  if (sem_result) {
    this->write_block(data);
    return true;
  }
  return false;
}

auto Async_Cache::try_read_block(uint8_t *target) -> bool {
  int sem_result;
  if (sem_getvalue(_read_sync, &sem_result)) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
  if (sem_result) {
    this->read_block(target);
    return true;
  }
  return false;
}

auto Async_Cache::unsafe_notify() -> void {
  if (sem_post(_write_sync)) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
  if (sem_post(_read_sync)) {
    throw std::system_error(std::error_code{errno, std::system_category()});
  }
}

} // namespace proto
