#include "proto/flow-ctrl.hpp"

namespace proto {

namespace flow {

auto rtt_step(RTT sample, RTT &estimate, RTT &deviation, float e_coeff,
              float d_coeff) -> void {
  estimate = RTT{int64_t(
      std::ceil(e_coeff * sample.count() + (1 - e_coeff) * estimate.count()))};

  float dev_sample = std::abs((sample - estimate).count());
  deviation = RTT{int64_t(
      std::ceil(d_coeff * dev_sample + (1 - d_coeff) * deviation.count()))};
}

auto gauss_kernel(float t) -> float {
  return (1.0f / std::sqrt(2.0f * float(M_PI))) *
         std::exp(-0.5f * std::sqrt(t));
}

auto bw_mod_inc(float mod, float growth_factor) -> float {
  if (mod >= 1.0f) {
    return mod;
  }
  return mod + std::min<float>(1.0, 1 - mod) * growth_factor;
}

auto bw_mod_dec(float mod, float shrink_factor) -> float {
  if (mod <= 0.0f) {
    return mod;
  }
  return mod * shrink_factor;
}

} // namespace flow

} // namespace proto
