#include "proto/packet.hpp"
#include "proto/bitfields.hpp"

#include <stdexcept>

#include <cmath>
#include <cstring>

namespace proto {

auto Base_Packet_Header::pack() const -> uint64_t {
  uint64_t result =
      util::pack_bitfield<uint64_t, 16, 0>(crc) |
      util::pack_bitfield<uint64_t, 44, 16>(uid) |
      util::pack_bitfield<uint64_t, 3, 60>(static_cast<uint8_t>(kind)) |
      util::pack_bitfield<uint64_t, 1, 63>(version);
  // result = util::to_little_endian(result);
  return result;
}
auto Base_Packet_Header::unpack(uint64_t value) -> Base_Packet_Header {
  auto he_value = value; // util::from_little_endian(value);
  return Base_Packet_Header{
      static_cast<uint16_t>(util::unpack_bitfield<uint64_t, 16, 0>(he_value)),
      static_cast<uint64_t>(util::unpack_bitfield<uint64_t, 44, 16>(he_value)),
      static_cast<Packet_Type>(
          util::unpack_bitfield<uint64_t, 3, 60>(he_value)),
      static_cast<bool>(util::unpack_bitfield<uint64_t, 1, 63>(he_value))};
}

auto CREQ_Packet_Header::pack() const -> uint64_t {
  return util::to_little_endian(
             util::pack_bitfield<uint64_t, 48, 0>(file_size)) |
         util::to_little_endian(
             util::pack_bitfield<uint64_t, 3, 48>(block_size)) |
         util::to_little_endian(util::pack_bitfield<uint64_t, 13, 51>(pad));
}
auto CREQ_Packet_Header::unpack(uint64_t value) -> CREQ_Packet_Header {
  auto he_value = util::from_little_endian(value);
  return CREQ_Packet_Header{
      static_cast<uint64_t>(util::unpack_bitfield<uint64_t, 48, 0>(he_value)),
      static_cast<uint8_t>(util::unpack_bitfield<uint64_t, 3, 48>(he_value)),
      static_cast<uint16_t>(util::unpack_bitfield<uint64_t, 13, 51>(he_value))};
}

auto RAUG_Packet_Header::pack() const -> uint64_t {
  return util::to_little_endian(
             util::pack_bitfield<uint64_t, 32, 0>(base_block)) |
         util::to_little_endian(util::pack_bitfield<uint64_t, 32, 32>(pad));
}
auto RAUG_Packet_Header::unpack(uint64_t value) -> RAUG_Packet_Header {
  auto he_value = util::from_little_endian(value);
  return RAUG_Packet_Header{
      static_cast<uint32_t>(util::unpack_bitfield<uint64_t, 32, 0>(he_value)),
      static_cast<uint32_t>(util::unpack_bitfield<uint64_t, 32, 32>(he_value))};
}

auto CREP_Packet_Header::pack() const -> uint64_t {
  return util::to_little_endian(util::pack_bitfield<uint64_t, 44, 0>(ruid)) |
         util::to_little_endian(util::pack_bitfield<uint64_t, 1, 44>(refuse)) |
         util::to_little_endian(util::pack_bitfield<uint64_t, 19, 45>(pad));
}
auto CREP_Packet_Header::unpack(uint64_t value) -> CREP_Packet_Header {
  auto he_value = util::from_little_endian(value);
  return CREP_Packet_Header{
      static_cast<UID>(util::unpack_bitfield<uint64_t, 44, 0>(he_value)),
      static_cast<bool>(util::unpack_bitfield<uint64_t, 1, 44>(he_value)),
      static_cast<uint32_t>(util::unpack_bitfield<uint64_t, 19, 45>(he_value))};
}

auto DAT_Packet_Header::pack() const -> uint64_t {
  return util::to_little_endian(util::pack_bitfield<uint64_t, 16, 0>(pad)) |
         util::to_little_endian(
             util::pack_bitfield<uint64_t, 16, 16>(burst_id)) |
         util::to_little_endian(
             util::pack_bitfield<uint64_t, 32, 32>(block_id));
}
auto DAT_Packet_Header::unpack(uint64_t value) -> DAT_Packet_Header {
  auto he_value = util::from_little_endian(value);
  return DAT_Packet_Header{
      static_cast<uint16_t>(util::unpack_bitfield<uint64_t, 16, 0>(he_value)),
      static_cast<uint16_t>(util::unpack_bitfield<uint64_t, 16, 16>(he_value)),
      static_cast<uint32_t>(util::unpack_bitfield<uint64_t, 32, 32>(he_value))};
}

auto ACK_Packet_Header::pack() const -> uint64_t {

  return util::to_little_endian(
             util::pack_bitfield<uint64_t, 32, 0>(block_id)) |
         util::to_little_endian(util::pack_bitfield<uint64_t, 32, 32>(ns_time));
}
auto ACK_Packet_Header::unpack(uint64_t value) -> ACK_Packet_Header {
  auto he_value = util::from_little_endian(value);
  return ACK_Packet_Header{
      static_cast<uint32_t>(util::unpack_bitfield<uint64_t, 32, 0>(he_value)),
      static_cast<uint32_t>(util::unpack_bitfield<uint64_t, 32, 32>(he_value))};
}

auto WAIT_Packet_Header::pack() const -> uint64_t {
  uint64_t result = 0;
  return result;
}
auto WAIT_Packet_Header::unpack(uint64_t) -> WAIT_Packet_Header {
  return WAIT_Packet_Header{0};
}

Packet::Packet() : _ready(false), _packed(false), _buffer() {
  _buffer.resize(16);
}

Packet::Packet(size_t block_size) : _ready(false), _packed(false), _buffer() {
  _buffer.resize(16 + block_size);
}

auto Packet::clear() -> void {
  _ready = false;
  _packed = false;
  memset(&base, 0, 16); // This is not good, but I'm doing it anyway.
  memset(_buffer.data(), 0, _buffer.size());
}

auto Packet::resize(size_t block_size) -> void {
  _buffer.resize(block_size + 16);
  _packed = false;
}

auto Packet::validate() -> bool {
  if (!_ready || !_packed) {
    throw std::invalid_argument(
        "Packet::validate called before packet was prepared.");
  }

  base.crc = util::from_little_endian(*reinterpret_cast<uint16_t *>(buffer()));
  return crc::validate_crc(buffer() + 2, buffer_size() - 2, base.crc);
}

auto Packet::generate_crc() -> void {
  if (!_ready || !_packed) {
    throw std::invalid_argument(
        "Packet::generate_crc called before packet was prepared.");
  }
  base.crc = crc::generate_crc(buffer() + 2, buffer_size() - 2);
  *reinterpret_cast<uint16_t *>(buffer()) = util::to_little_endian(base.crc);
}

auto Packet::pack() -> void {
  if (_packed) {
    return;
  }
  if (!_ready) {
    throw std::invalid_argument(
        "Packet::pack called before packet was prepared.");
  }

  *reinterpret_cast<uint64_t *>(_buffer.data()) = base.pack();
  switch (base.kind) {
  case Packet_Type::CREQ:
    *(reinterpret_cast<uint64_t *>(_buffer.data()) + 1) = creq_dat.pack();
    break;
  case Packet_Type::RAUG:
    *(reinterpret_cast<uint64_t *>(_buffer.data()) + 1) = raug_dat.pack();
    break;
  case Packet_Type::CREP:
    *(reinterpret_cast<uint64_t *>(_buffer.data()) + 1) = crep_dat.pack();
    break;
  case Packet_Type::DAT:
    *(reinterpret_cast<uint64_t *>(_buffer.data()) + 1) = dat_dat.pack();
    break;
  case Packet_Type::ACK:
    *(reinterpret_cast<uint64_t *>(_buffer.data()) + 1) = ack_dat.pack();
    break;
  case Packet_Type::WAIT:
    *(reinterpret_cast<uint64_t *>(_buffer.data()) + 1) = wait_dat.pack();
    break;
  case Packet_Type::PAD:
    break;
  case Packet_Type::PAD2:
    break;
  }
  _packed = true;
}

auto Packet::unpack() -> void {
  base = base.unpack(*reinterpret_cast<uint64_t *>(_buffer.data()));
  switch (base.kind) {
  case Packet_Type::CREQ:
    creq_dat =
        creq_dat.unpack(*(reinterpret_cast<uint64_t *>(_buffer.data()) + 1));
    break;
  case Packet_Type::RAUG:
    raug_dat =
        raug_dat.unpack(*(reinterpret_cast<uint64_t *>(_buffer.data()) + 1));
    break;
  case Packet_Type::CREP:
    crep_dat =
        crep_dat.unpack(*(reinterpret_cast<uint64_t *>(_buffer.data()) + 1));
    break;
  case Packet_Type::DAT:
    dat_dat =
        dat_dat.unpack(*(reinterpret_cast<uint64_t *>(_buffer.data()) + 1));
    break;
  case Packet_Type::ACK:
    ack_dat =
        ack_dat.unpack(*(reinterpret_cast<uint64_t *>(_buffer.data()) + 1));
    break;
  case Packet_Type::WAIT:
    wait_dat =
        wait_dat.unpack(*(reinterpret_cast<uint64_t *>(_buffer.data()) + 1));
    break;
  case Packet_Type::PAD:
    break;
  case Packet_Type::PAD2:
    break;
  }
  _packed = true;
  _ready = true;
}

auto Packet::buffer_size() const -> size_t {
  return (base.kind == Packet_Type::DAT) ? _buffer.size() : 16;
}

auto Packet::buffer() -> uint8_t * { return _buffer.data(); }

auto Packet::block_size() const -> size_t { return _buffer.size() - 16; }

auto Packet::block_ptr() -> uint8_t * { return _buffer.data() + 12; }

auto Packet::load_creq(UID uid, size_t file_size, uint8_t block_size) -> void {
  base = Base_Packet_Header{0, uid, Packet_Type::CREQ, false};
  creq_dat = CREQ_Packet_Header{file_size, block_size, 0};
  _packed = false;
  _ready = true;
}

auto Packet::load_raug(UID uid, uint32_t base_block) -> void {
  base = Base_Packet_Header{0, uid, Packet_Type::RAUG, false};
  raug_dat = RAUG_Packet_Header{base_block, 0};
  _packed = false;
  _ready = true;
}

auto Packet::load_crep(UID uid, UID ruid, bool refuse) -> void {
  base = Base_Packet_Header{0, uid, Packet_Type::CREP, false};
  crep_dat = CREP_Packet_Header{ruid, refuse, 0};
  _packed = false;
  _ready = true;
}

auto Packet::load_dat(UID uid, uint16_t burst_id) -> void {
  base = Base_Packet_Header{0, uid, Packet_Type::DAT, false};
  dat_dat.pad = 0;
  dat_dat.burst_id = burst_id;
  dat_dat.block_id =
      util::from_little_endian(*reinterpret_cast<uint32_t *>(block_ptr()));
  _packed = false;
  _ready = true;
}

auto Packet::load_ack(UID uid, uint32_t block_id, uint32_t ns_time) -> void {
  base = Base_Packet_Header{0, uid, Packet_Type::ACK, false};
  ack_dat = ACK_Packet_Header{block_id, ns_time};
  _packed = false;
  _ready = true;
}

auto Packet::load_wait(UID uid) -> void {
  base = Base_Packet_Header{0, uid, Packet_Type::WAIT, false};
  wait_dat = WAIT_Packet_Header{0};
  _packed = false;
  _ready = true;
}

auto CREQ_Packet(UID uid, size_t file_size, uint8_t block_size) -> Packet {
  Packet result{};
  result.load_creq(uid, file_size, block_size);
  return result;
}

auto RAUG_Packet(UID uid, uint32_t base_block) -> Packet {
  Packet result{};
  result.load_raug(uid, base_block);
  return result;
}

auto CREP_Packet(UID uid, UID ruid, bool refuse) -> Packet {
  Packet result{};
  result.load_crep(uid, ruid, refuse);
  return result;
}

auto DAT_Packet(UID uid, uint16_t burst_id, size_t block_size) -> Packet {
  Packet result{block_size};
  result.load_dat(uid, burst_id);
  return result;
}

auto ACK_Packet(UID uid, uint32_t block_id, uint32_t ns_time) -> Packet {
  Packet result{};
  result.load_ack(uid, block_id, ns_time);
  return result;
}

auto WAIT_Packet(UID uid) -> Packet {
  Packet result{};
  result.load_wait(uid);
  return result;
}

} // namespace proto
