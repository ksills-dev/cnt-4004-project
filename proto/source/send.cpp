#include "proto/send.hpp"

#include "proto/async-cache.hpp"
#include "proto/bitfields.hpp"
#include "proto/flow-ctrl.hpp"
#include "proto/maybe.hpp"
#include "proto/packet-socket.hpp"
#include "proto/packet.hpp"
#include "proto/partition.hpp"

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#ifndef NDEBUG
#include <glog/logging.h>
#endif

namespace proto {

namespace {

struct Burst {
  uint16_t burst_id;
  uint32_t first_id;
  uint32_t last_id;
  // uint32_t send_time;
  std::unordered_set<uint32_t> tracking;
  std::chrono::time_point<std::chrono::high_resolution_clock> sent_at;
  std::chrono::time_point<std::chrono::high_resolution_clock> back_at;

  bool first_got = false;
  bool last_got = false;
  size_t n_got = 0;
  uint32_t low_time = std::numeric_limits<uint32_t>::max();
  uint32_t high_time = std::numeric_limits<uint32_t>::min();
};
using Burst_Queue = std::unordered_map<uint16_t, Burst>;

static float const default_loss_rate = 0.0;
static size_t const default_cache_size = 512;
static size_t const default_block_size = 1024;
static size_t const default_max_wait_string =
    std::numeric_limits<size_t>::max();
static size_t const default_max_connect_attempts = 3;
static size_t const default_max_transfer_attempts = 3;
static size_t const default_max_term_attempts = 1;
static std::chrono::milliseconds const default_connect_timeout =
    std::chrono::milliseconds{3000};
static std::chrono::milliseconds const default_transfer_timeout =
    std::chrono::milliseconds{3000};
static std::chrono::milliseconds const default_burst_length =
    std::chrono::milliseconds{100};
static flow::RTT const default_initial_rtte = std::chrono::milliseconds{1000};
static flow::RTT const default_initial_rttd = std::chrono::milliseconds{500};

auto listen_job(Packet_Socket &socket, flow::RTT timeout, size_t max_attempts,
                size_t max_wait_string, uint64_t suid, Partition_List &gap_list,
                Burst_Queue &burst_queue, std::mutex &queue_lock,
                std::atomic_bool &kill_flag, std::atomic_int &attempts,
                int loss) -> void {
#ifndef NDEBUG
  DLOG(INFO) << "Listen job started.";
#endif
  std::vector<uint8_t> buffer{};
  Packet in_packet{16};
  size_t wait_string = 0;
  std::srand(
      std::chrono::high_resolution_clock::now().time_since_epoch().count());
  int dropped = 0;

  auto expire_time = std::chrono::high_resolution_clock::now() + timeout;
  while (not kill_flag and not gap_list.empty()) {
    if (attempts >= static_cast<int>(max_attempts)) {
#ifndef NDEBUG
      DLOG(WARNING) << "Listen job exceeded maximum attempts.";
#else
      std::cout << "Sender waiting too long, Requesting exit...\n";
#endif
      kill_flag = true;
      break;
    }
    { // New scope for a lock guard
      std::lock_guard<std::mutex> lock{queue_lock};
      if (burst_queue.empty()) {
        continue;
      }
    }

    if (!socket
             .fetch(
                 in_packet, ACK | WAIT,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     expire_time - std::chrono::high_resolution_clock::now()))
             .exists() ||
        in_packet.base.uid != suid) {
      if (std::chrono::high_resolution_clock::now() > expire_time) {
#ifndef NDEBUG
        DLOG(INFO) << "Listen job timed out.";
#endif
        attempts += 1;
        expire_time = std::chrono::high_resolution_clock::now() + timeout;
      }
      continue;
    }
    auto rand = std::rand() % 100;
    if (rand < loss) {
      dropped++;
      // Simulating packet loss.
      continue;
    }
    attempts = 0;

    expire_time = std::chrono::high_resolution_clock::now() + timeout;
    if (in_packet.base.kind == Packet_Type::ACK) {
      auto block_id = in_packet.ack_dat.block_id;
      auto ns_time = in_packet.ack_dat.ns_time;
      { // New scope for a lock guard
        std::lock_guard<std::mutex> lock{queue_lock};
        for (auto &pair : burst_queue) {
          auto &burst = pair.second;
          if (not burst.tracking.count(block_id)) {
            continue;
          }
          burst.tracking.erase(block_id);
          if (burst.n_got++ == 0) {
            burst.back_at = std::chrono::high_resolution_clock::now();
          }
          if (block_id == burst.first_id) {
            burst.first_got = true;
          }
          if (block_id == burst.last_id) {
            burst.last_got = true;
          }
          burst.low_time = std::min(burst.low_time, ns_time);
          burst.high_time = std::max(burst.high_time, ns_time);
        }
      }
      gap_list.satisfy(block_id);

    } else {
      if (++wait_string >= max_wait_string) {
#ifndef NDEBUG
        DLOG(WARNING) << "Listen job found too many wait packets.";
#else
        std::cout << "Found too many WAIT Packets... Requesting Exit.\n";
#endif
        kill_flag = true;
        break;
      }
    }
  }

#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "Listener dropped " << dropped << " packets.\n";
#ifndef NDEBUG
  DLOG(INFO) << "Listen job complete.";
#endif
}

auto cache_job(std::FILE *file, uint32_t initial_block, size_t block_size,
               Async_Cache &cache, Partition_List &gap_list,
               std::atomic_bool &kill_flag) -> void {
#ifndef NDEBUG
  DLOG(INFO) << "Sender caching job started.";
#endif
  std::vector<uint8_t> buffer{};
  buffer.reserve(4 + block_size);

  uint32_t block_id;
  uint32_t previous_block = std::numeric_limits<uint32_t>::max();
  while (block_id = gap_list.next(),
         (not kill_flag and block_id != std::numeric_limits<uint32_t>::max())) {
    std::memset(buffer.data(), 0, buffer.size());
    if (previous_block != (block_id - 1)) {
      std::fseek(file, (block_id - initial_block) * block_size, SEEK_SET);
    }
    previous_block = block_id;
    std::fread(buffer.data() + 4, 1, block_size, file);
    *(uint32_t *)buffer.data() = util::to_little_endian(block_id);
    cache.write_block(buffer.data());
  }
#ifndef NDEBUG
  DLOG(INFO) << "Cache job complete, closing file...";
#endif
  std::fclose(file);
}

} // namespace

Send_Params::Send_Params()
    : _loss(default_loss_rate), _cache_size(default_cache_size),
      _block_size(default_block_size),
      _max_wait_string(default_max_wait_string),
      _max_connect_attempts(default_max_connect_attempts),
      _max_transfer_attempts(default_max_transfer_attempts),
      _max_term_attempts(default_max_term_attempts),
      _connect_timeout(default_connect_timeout),
      _transfer_timeout(default_transfer_timeout),
      _burst_length(default_burst_length), _initial_rtte(default_initial_rtte),
      _initial_rttd(default_initial_rttd) {}

auto send_file(std::string const &filename, uint16_t my_port,
               uint16_t peer_port, std::string const &receiver_ip,
               Send_Params const &options) -> Send_Result {
// Setup
#ifndef NDEBUG
  DLOG(INFO) << "Preparing resources for transfer.";
#endif

  Packet_Socket socket{Socket_Address{Socket_Address_Type::Ip4, my_port}};
  Socket_Address target{Socket_Address_Type::Ip4, peer_port};
  try {
    target.addr(receiver_ip);
  } catch (std::invalid_argument &) {
#ifndef NDEBUG
    DLOG(WARNING) << "Bad address was provided!";
#else
    std::cout << "User provided a bad peer IP Address!\n";
#endif
    return Send_Result::Bad_Address;
  }

  // Please don't do this in production code!
  std::srand(
      std::chrono::high_resolution_clock::now().time_since_epoch().count());
  uint64_t suid = uint64_t(std::rand()) % ((uint64_t)1 << 44);
  uint64_t ruid;

  auto file = std::fopen(filename.c_str(), "r");
  if (file == nullptr) {
#ifndef NDEBUG
    DLOG(WARNING) << "Could not open filename for read!";
#else
    std::cout << "User provided a bad infile!\n";
#endif
    return Send_Result::System_Error;
  }
  fseek(file, 0, SEEK_END);
  uint64_t file_size = uint64_t(std::ftell(file));
  fseek(file, 0, SEEK_SET);

  auto block_size = options.block_size();
  uint32_t n_blocks = (file_size / options.block_size()) +
                      ((file_size % options.block_size()) ? 1 : 0);

  Async_Cache cache{options.cache_size(), size_t(options.block_size() + 4)};
  uint32_t initial_block =
      1 + (uint32_t(std::rand()) %
           (std::numeric_limits<uint32_t>::max() - n_blocks - 1));

  Partition_List gap_list{initial_block, n_blocks};

  std::atomic_bool kill_cache{false};
  std::thread cache_thread{cache_job,           file,
                           initial_block,       options.block_size(),
                           std::ref(cache),     std::ref(gap_list),
                           std::ref(kill_cache)};

#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "[Connection Parameters]"        //
      << "\n\tSUID: " << suid             //
      << "\n\tTarget IP: " << receiver_ip //
      << "\n\tTarget Port: " << peer_port //
      << "\n\tSource Port: " << my_port   //
      << "\n\tInFile: " << filename       //
      << "\n\tFilesize: " << file_size    //
      << "\n\tBlock size: " << options.block_size() << "(2^"
      << static_cast<uint8_t>(std::log2(options.block_size())) - 7 << ")" //
      << "\n\tBase Block: " << initial_block                              //
      << "\n\t# Blocks: " << n_blocks << "\n";

#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "Reaching out for Handshake...\n";

  auto handshake_time = std::chrono::high_resolution_clock::now();

  std::atomic_int attempts{0};
  std::chrono::time_point<std::chrono::high_resolution_clock> expire_time =
      std::chrono::high_resolution_clock::now() + options.connect_timeout();

  auto creq_packet =
      CREQ_Packet(suid, file_size, uint8_t(std::log2(block_size)) - 7);
  auto raug_packet = RAUG_Packet(suid, initial_block - 1);

  flow::RTT rtte;
  flow::RTT rttd;
  float initial_bwe;
  while (true) {
    if (std::chrono::high_resolution_clock::now() >= expire_time) {
#ifndef NDEBUG
      DLOG(WARNING) << "Connect has exceeded timeout.";
#endif
      kill_cache = true;
      cache.unsafe_notify();
      cache_thread.join();
      return Send_Result::Connect_Timeout;
    }
    if (attempts >= static_cast<int>(options.max_connect_attempts())) {
#ifndef NDEBUG
      DLOG(WARNING)
#else
      std::cout
#endif
          << "Connect has exceeded max attempts.\n";
      kill_cache = true;
      cache.unsafe_notify();
      cache_thread.join();
      return Send_Result::Connect_Exceeded_Attempts;
    }

#ifndef NDEBUG
    DLOG(INFO) << "Sending Connection Request Packets.";
#endif
    socket.send(creq_packet, target);
    socket.send(raug_packet, target);
    auto away_time = std::chrono::high_resolution_clock::now();

    Packet response{};
    if (!socket
             .fetch(
                 response, CREP,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     expire_time - std::chrono::high_resolution_clock::now()))
             .exists() ||
        response.base.uid != suid) {
#ifndef NDEBUG
      DLOG(WARNING) << "Did not receive valid CREP.";
#endif
      expire_time =
          std::chrono::high_resolution_clock::now() + options.connect_timeout();
      attempts += 1;
      continue;
    }
    expire_time =
        std::chrono::high_resolution_clock::now() + options.connect_timeout();
    ruid = response.crep_dat.ruid;
    if (response.crep_dat.refuse) {
#ifndef NDEBUG
      DLOG(WARNING)
#else
      std::cout
#endif
          << "Connection was refused.\n";
      attempts += 1;
      continue;
    }
#ifndef NDEBUG
    DLOG(INFO) << "Received valid CREP, looking for CACK.";
#endif
    if (!socket
             .fetch(
                 response, ACK,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     expire_time - std::chrono::high_resolution_clock::now()))
             .exists() ||
        response.base.uid != suid ||
        response.ack_dat.block_id != initial_block - 1) {
#ifndef NDEBUG
      DLOG(WARNING) << "Did not receive valid CACK.";
#endif
      expire_time =
          std::chrono::high_resolution_clock::now() + options.connect_timeout();
      attempts += 1;
      continue;
    }
    auto back_time = std::chrono::high_resolution_clock::now();
    auto ns_time = response.ack_dat.ns_time;

#ifndef NDEBUG
    DLOG(INFO) << "Received valid CACK.";
#endif
    rtte = std::chrono::duration_cast<std::chrono::nanoseconds>(back_time -
                                                                away_time);
#ifndef NDEBUG
    DLOG(INFO) << "Given ns_time: " << ns_time;
#endif
    initial_bwe = float((double(16.0) / block_size) /
                        (double(ns_time) / 1000000.0)); // Blocks/ms
    break;
  }
  rttd = flow::RTT{rtte.count() / 2};

#ifndef NDEBUG
  DLOG(INFO) << "Post-Handshake Setup.";
#endif
  flow::BW_Estimator<> estimator{initial_bwe};
  float bw_mod = float(0.1);
  uint16_t burst_id = 0;

  Burst_Queue burst_queue{};
  std::mutex queue_lock{};

#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "[Transfer Parameters]"                              //
      << "\n\tRUID: " << ruid                                 //
      << "\n\tInitial RTTE: " << rtte.count() / 1000 << " us" //
      << "\n\tInitial RTTD: " << rttd.count() / 1000 << " us" //
      << "\n\tInitial BWE: " << estimator.estimate()
      << " blocks per millisecond."
      << "\n\tInitial BW_Mod: " << bw_mod //
      << "\n\tInitial Burst_ID: " << burst_id << "\n";
#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "Starting Transfer Phase...\n";

  auto transfer_time = std::chrono::high_resolution_clock::now();

  // TODO: Start listener
  attempts = 0;
  std::atomic_bool kill_listen{false};
  std::thread listen_thread{listen_job,
                            std::ref(socket),
                            options.transfer_timeout(),
                            options.max_transfer_attempts(),
                            options.max_wait_string(),
                            suid,
                            std::ref(gap_list),
                            std::ref(burst_queue),
                            std::ref(queue_lock),
                            std::ref(kill_listen),
                            std::ref(attempts),
                            100 * static_cast<int>(options.loss())};

  int congestion_window = 1;
  int away_bursts = 0;
  int skipped_bursts = 0;

  Packet send_packet{block_size};
  Packet wait_packet = WAIT_Packet(ruid);
  auto next_burst_time = std::chrono::high_resolution_clock::now();
  while (true) {
    auto current_timeout = flow::RTT{int(std::exp2(attempts.load())) *
                                     (rtte.count() + rttd.count() * 4)} +
                           options.burst_length();
    if (gap_list.empty()) {
#ifndef NDEBUG
      DLOG(INFO) << "All blocks acknowledged, moving to terminate.";
#endif
      kill_cache = true;
      cache.unsafe_notify();
      cache_thread.join();
      listen_thread.join();
      break;
    }
    if (kill_listen) {
      // If we're here, the listen thread did this.
#ifndef NDEBUG
      DLOG(INFO) << "Listener requested exit.";
#endif
      kill_cache = true;
      cache.unsafe_notify();
      cache_thread.join();
      listen_thread.join();
      return Send_Result::Transfer_Timeout;
    }

    { // Scope for lock guard
      std::vector<uint16_t> marked;
      std::lock_guard<std::mutex> lock{queue_lock};
      for (auto const &pair : burst_queue) {
        auto const &burst_id = pair.first;
        auto const &burst = pair.second;
        if (burst.tracking.empty() ||
            burst.sent_at + current_timeout <
                std::chrono::high_resolution_clock::now()) {
#ifndef NDEBUG
          DLOG(INFO) << "Cleaning Burst " << burst.burst_id //
                     << "\n\tMissing: " << burst.tracking.size();
#endif

          away_bursts -= 1;
          if (burst.n_got > 0) {
            flow::rtt_step(burst.back_at - burst.sent_at, rtte, rttd);
#ifndef NDEBUG
            DLOG(INFO)
                << "New RTT: "
                << std::chrono::duration_cast<std::chrono::microseconds>(rtte)
                       .count()
                << " +- "
                << std::chrono::duration_cast<std::chrono::microseconds>(rttd)
                       .count();
#endif
          }
          if (burst.tracking.size() == 0 && skipped_bursts > 0) {
            congestion_window += 1;
            skipped_bursts = 0;
          } else if (burst.n_got == 0) {
            congestion_window = std::max(1, congestion_window - 1);
            skipped_bursts = 0;
          }
          if (burst.n_got >= 2) {
            estimator.refine(
                float(burst.n_got + burst.tracking.size()) /
                (float(burst.high_time - burst.low_time) / 1000000));
#ifndef NDEBUG
            DLOG(INFO) << "New BWE: " << estimator.estimate()
                       << " blocks per millisecond";
#endif
          }

          if (burst.first_got && burst.last_got) {
            bw_mod = flow::bw_mod_inc(bw_mod);
          } else {
            bw_mod = flow::bw_mod_dec(bw_mod);
          }
#ifndef NDEBUG
          DLOG(INFO) << "New BW_Mod: " << bw_mod;
#endif
          marked.push_back(burst_id);
        }
      }
      for (auto const &mark : marked) {
        burst_queue.erase(mark);
      }
    }

    auto burst_start = std::chrono::high_resolution_clock::now();
    if (away_bursts < congestion_window) {
      if (burst_start >= next_burst_time) {
        size_t desired_burst_size = // std::numeric_limits<size_t>::max();
            static_cast<size_t>(options.burst_length().count() * bw_mod *
                                estimator.estimate());
        desired_burst_size = (desired_burst_size > 2) ? desired_burst_size : 2;

        Burst burst_placeholder{};
        // burst_placeholder.tracking.reserve(desired_burst_size);
        burst_placeholder.tracking = decltype(Burst::tracking){};
        burst_placeholder.burst_id = burst_id;
        burst_placeholder.first_id = std::numeric_limits<uint32_t>::max();
        burst_placeholder.last_id = std::numeric_limits<uint32_t>::max();
        burst_placeholder.first_got = false;
        burst_placeholder.last_got = false;
        burst_placeholder.n_got = 0;
        burst_placeholder.sent_at = std::chrono::high_resolution_clock::now();

        queue_lock.lock();
        burst_queue.insert(
            std::make_pair(burst_id, std::move(burst_placeholder)));
        queue_lock.unlock();

#ifndef NDEBUG
        DLOG(INFO) << "Preparing burst " << burst_id;
#endif
        size_t actual_burst_size = 0;
        for (; actual_burst_size < desired_burst_size; actual_burst_size++) {
          if (std::chrono::high_resolution_clock::now() - burst_start >=
              options.burst_length()) {
#ifndef NDEBUG
            DLOG(INFO) << "Burst ran out of time, cutting short.";
#endif
            break;
          }
          if (cache.try_read_block(send_packet.block_ptr())) {
            send_packet.load_dat(ruid, burst_id);
            {
              std::lock_guard<std::mutex> lock{queue_lock};
              if (std::find(burst_queue[burst_id].tracking.begin(),
                            burst_queue[burst_id].tracking.end(),
                            send_packet.dat_dat.block_id) !=
                  burst_queue[burst_id].tracking.end()) {
#ifndef NDEBUG
                DLOG(INFO) << "Loopback, shrinking burst.";
#endif
                break;
              }
              if (gap_list.needs(send_packet.dat_dat.block_id) == false) {
                actual_burst_size -= 1;
                continue;
              }
              if (send_packet.dat_dat.block_id < initial_block ||
                  send_packet.dat_dat.block_id >= initial_block + n_blocks) {
#ifndef NDEBUG
                DLOG(WARNING) << "Plz no.";
#endif
              }
              burst_queue[burst_id].tracking.insert(
                  send_packet.dat_dat.block_id);
              burst_queue[burst_id].last_id = send_packet.dat_dat.block_id;
              burst_queue[burst_id].last_got = false;
              if (burst_queue[burst_id].first_id ==
                  std::numeric_limits<uint32_t>::max()) {
                burst_queue[burst_id].first_id = send_packet.dat_dat.block_id;
              }
            }
            socket.send(send_packet, target);
          } else {
#ifndef NDEBUG
            DLOG(INFO) << "Waiting on cache, shrinking burst.";
#endif
            break;
          }
        }

        if (actual_burst_size != 0) {
          burst_id++;
          away_bursts += 1;
        } else {
#ifndef NDEBUG
          DLOG(INFO) << "Empty Burst, Skipping...";
#endif
          burst_queue.erase(burst_id);
        }
      }
    } else {
      skipped_bursts += 1;
    }
    next_burst_time = burst_start + options.burst_length();
    std::this_thread::sleep_until(next_burst_time);
  }

#ifndef NDEBUG
  DLOG(INFO)
#else
  std::cout
#endif
      << "Starting Termination Phase...\n";
  auto term_time = std::chrono::high_resolution_clock::now();
  expire_time =
      std::chrono::high_resolution_clock::now() +
      flow::RTT{(std::chrono::duration_cast<flow::RTT>(options.burst_length())
                     .count() *
                 2)};
  auto tack_packet = ACK_Packet(ruid, initial_block + n_blocks, 0);
  bool send_tack = true;
  attempts = 0;
  Packet tack_response{};
  while (true) {
    if (std::chrono::high_resolution_clock::now() >= expire_time) {
      attempts += 1;
      send_tack = true;
    }
    if (attempts >= static_cast<int>(options.max_term_attempts())) {
#ifndef NDEBUG
      DLOG(WARNING) << "Did not receive TACK response, but assuming success.";
#endif
      break;
    }
    if (send_tack) {
#ifndef NDEBUG
      DLOG(INFO) << "Sending out TACK.";
#endif
      send_tack = false;
      socket.send(tack_packet, target);
    }
    if (!socket
             .fetch(
                 tack_response, ACK,
                 std::chrono::duration_cast<std::chrono::milliseconds>(
                     expire_time - std::chrono::high_resolution_clock::now()))
             .exists() ||
        tack_response.base.uid != suid ||
        tack_response.ack_dat.block_id != initial_block + n_blocks) {
#ifndef NDEBUG
      DLOG(INFO) << "Ignoring irrelevant ACK.";
#endif
      continue;
    }
#ifndef NDEBUG
    DLOG(INFO)
#else
    std::cout
#endif
        << "Valid TACK response received, all done!\n\n";
    break;
  }

  std::cout
      << "Ended with an RTT of "
      << std::chrono::duration_cast<std::chrono::microseconds>(rtte).count()
      << "us +- "
      << std::chrono::duration_cast<std::chrono::microseconds>(rttd).count()
      << "us.\n";
  std::cout << "Ended with a BW Estimate of "
            << estimator.estimate() * block_size * 8 << "kbps.\n";
  std::cout << "Ended with a Congestion Window of " << congestion_window
            << " bursts.\n";
  std::cout << "Ended with a BW Modifier of " << bw_mod << std::endl;
  std::cout << std::endl;
  std::cout << "Handshake took "
            << std::chrono::duration_cast<std::chrono::milliseconds>(
                   transfer_time - handshake_time)
                   .count()
            << " ms\n";
  std::cout << "Transfer took "
            << std::chrono::duration_cast<std::chrono::milliseconds>(
                   term_time - transfer_time)
                   .count()
            << " ms\n";
  std::cout << "Termination took "
            << std::chrono::duration_cast<std::chrono::milliseconds>(
                   std::chrono::high_resolution_clock::now() - term_time)
                   .count()
            << " ms\n";

  return Send_Result::Success;
} // namespace proto

} // namespace proto

